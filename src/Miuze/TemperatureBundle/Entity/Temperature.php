<?php

namespace Miuze\TemperatureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Miuze\TemperatureBundle\Repository\TemperatureRepository")
 * @ORM\Table(name="temperature")
 */
class Temperature
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     */
    private $address;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     */
    private $value;

    /**
     * @ORM\OneToMany(
     *      targetEntity = "Miuze\RoomBundle\Entity\ViewItem",
     *      mappedBy = "temperature",
     * )
     */
    private $view;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Temperature
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address.
     *
     * @param string $address
     *
     * @return Temperature
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address.
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set value.
     *
     * @param string|null $value
     *
     * @return Temperature
     */
    public function setValue($value = null)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value.
     *
     * @return string|null
     */
    public function getValue()
    {
        return $this->value;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->view = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add view.
     *
     * @param \Miuze\RoomBundle\Entity\ViewItem $view
     *
     * @return Temperature
     */
    public function addView(\Miuze\RoomBundle\Entity\ViewItem $view)
    {
        $this->view[] = $view;

        return $this;
    }

    /**
     * Remove view.
     *
     * @param \Miuze\RoomBundle\Entity\ViewItem $view
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeView(\Miuze\RoomBundle\Entity\ViewItem $view)
    {
        return $this->view->removeElement($view);
    }

    /**
     * Get view.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getView()
    {
        return $this->view;
    }
}
