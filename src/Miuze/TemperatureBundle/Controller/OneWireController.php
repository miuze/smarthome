<?php

namespace Miuze\TemperatureBundle\Controller;

use Miuze\TemperatureBundle\Controller\DefaultController;
use Miuze\TemperatureBundle\Entity\Temperature;
use Miuze\TemperatureBundle\Form\OneWire\SensorType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
* @Route("/admin/temperature")
*/
class OneWireController extends DefaultController
{
    public $defaultRoutePlc = 'miuze_temperature_onewire_index';

    /**
     * @Route(
     *      "/",
     *      name="miuze_temperature_onewire_index"
     * )
     */
    public function indexAction()
    {
        $list = $this->getDoctrine()->getRepository('MiuzeTemperatureBundle:Temperature')->findAll();

        return $this->view(
            '@MiuzeTemperature/OneWire/index.html.twig',
            [
                'action' => 'Lista czujników',
                'list' => $list,
            ]
        );
    }
    
    /**
     * @Route(
     *      "/add",
     *      name="miuze_temperature_onewire_add"
     * )
     */
    public function addAction(Request $request)
    {
        $entity = new Temperature();
        $form = $this->createForm(SensorType::class, $entity, array(
            'temperature_repository' => $this->getDoctrine()->getRepository('MiuzeTemperatureBundle:Temperature')
        ));
        if($request->isMethod(Request::METHOD_POST)){
            $form->handleRequest($request);
            if($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                $this->setMessage('success', 'Zapisano ustawienia');

                return $this->redirect($this->generateUrl($this->defaultRoutePlc));
            }
        }

        return $this->view(
            '@MiuzeTemperature/OneWire/add.html.twig',
            [
                'action' => 'Dodaj czujnik',
                'form' => $form->createView(),
            ]
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name="miuze_temperature_onewire_edit"
     * )
     */
    public function editAction(Request $request)
    {
        $entity = $this->getDoctrine()
            ->getRepository('MiuzeTemperatureBundle:Temperature')
            ->findOneById($request->attributes->getInt('id'));
        if($entity == null){
            $this->setMessage('danger', 'Nie znaleziono czujnika');

            return $this->redirect($this->generateUrl($this->defaultRoutePlc));
        }
        $form = $this->createForm(SensorType::class, $entity);
        if($request->isMethod(Request::METHOD_POST)){
            $form->handleRequest($request);
            if($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $em->merge($entity);
                $em->flush();
                $this->setMessage('success', 'Zapisano ustawienia');

                return $this->redirect($this->generateUrl($this->defaultRoutePlc));
            }
        }

        return $this->view(
            '@MiuzeTemperature/OneWire/add.html.twig',
            [
                'action' => 'Edycja Czujnika',
                'form' => $form->createView(),
            ]
        );
    }
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="miuze_temperature_onewire_delete"
     * )
     */
    public function deleteAction(Request $request)
    {
        $entity = $this->getDoctrine()
            ->getRepository('MiuzeTemperatureBundle:Temperature')
            ->findOneById($request->attributes->getInt('id'));
        if($entity == null){
            $this->setMessage('danger', 'Nie znaleziono czujnika');

            return $this->redirect($this->generateUrl($this->defaultRoutePlc));
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();
        $this->setMessage('success', 'Czujnik usunięty');

        return $this->redirect($this->generateUrl($this->defaultRoutePlc));
    }
}
