<?php

namespace Miuze\TemperatureBundle\Form\OneWire;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Miuze\TemperatureBundle\Entity\Temperature;

class SensorType extends AbstractType
{

    /**
     * @var array sensors
     */
    private $sensors;

    private $oneWirePath = __DIR__ . '/../../../../../var/1wire';

    public function getName()
    {
        return 'miuze_temperature_onewire';
    }

    function buildForm(FormBuilderInterface $builder, array $options)
    {
//        montowanie czujników
//        sudo owfs -C -d /dev/ttyUSB0 -m /mnt/1wire
//        sudo owfs -C -d /dev/serial/by-id/usb-MERA-PROJEKT_USB__-__1Wire__MP00206__MPXWO8KB-if00-port0 -m /mnt/1wire --allow_other
        $this->loadSensors();
        $this->getNotExistDbSensors($options['temperature_repository']->findAll());

        $builder
            ->add(
                'name',
                TextType::class,
                array(
                    'label' => 'Nazwa punktu pomiarowego',
                    'attr' => array(
                        'placeholder' => 'Nazwa punktu pomiarowego',
                    ),
                )
            )
            ->add(
                'address',
                ChoiceType::class,
                array(
                    'label' => 'Kod czujnika',
                    'choices' => $this->sensors,
                    'placeholder' => 'Wybierz czujnik',
                    'required' => true,
                )
            )
            ->add(
                'submit',
                SubmitType::class,
                array(
                    'label' => 'Zapisz',
                    'attr' => array(
                        'class' => 'btn btn-success'
                    )
                )
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            array(
                'data_class' => Temperature::class,
                'temperature_repository' => null,
            )
        );
    }

    private function loadSensors()
    {
        foreach (scandir($this->oneWirePath) as $file) {
            if ($file != '.' && $file != '..') {
                $pattern = '/^28.([a-zA-Z0-9]+)/';
                if (preg_match($pattern, $file)) {
                    $this->setSensor($file);
                }
            }
        }
    }

    private function getCurrencyTemperature(): void
    {
        $optionsList = [];
        foreach ($this->sensors as $sensor) {
//            dump(__DIR__);die;
            $temp = round(floatval(shell_exec('cat '. $this->oneWirePath . '/' . $sensor . '/temperature')), 1);
            $optionsList[sprintf('ID: %s - Temperatura: %g °C', $sensor, $temp)] = $sensor;
        }
        $this->sensors = $optionsList;
    }

    private function setSensor(string $file){
        $this->sensors[$file] = $file;
    }

    private function getNotExistDbSensors($dbSensors)
    {
        $sensorsExist = [];
        foreach ($dbSensors as $item) {
            $sensorsExist[$item->getAddress()] = $item->getAddress();
        }
        if ($this->sensors != null || $sensorsExist != null) {
            $this->sensors = array_diff($this->sensors, $sensorsExist);
        } else {
            $this->sensors = [];
        }
        $this->getCurrencyTemperature();
    }
}
