<?php

declare(strict_types=1);

namespace Miuze\TemperatureBundle\Twig\Extension;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Miuze\TemperatureBundle\Entity\Temperature;

class TemperatureExtension extends \Twig_Extension
{
    /**
     * @var \Doctrine\Bundle\DoctrineBundle\Registry
     */
    private $doctrine;

    function __construct(Registry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function getName()
    {
        return 'miuze_temperature_onewire_extension';
    }

    public function getFunctions(): array
    {
        return array(
            new \Twig_SimpleFunction(
                'render_temperature', [$this, 'renderTemperature'],
                [
                    'is_safe' => ['html'],
                    'needs_environment' => true,
                ]
            ),

        );
    }

    public function renderTemperature(\Twig_Environment $environment, Temperature $temperature)
    {
        $repo = $this->doctrine->getRepository('MiuzeTemperatureBundle:Temperature');
        $temperature = $repo->findOneBy(['id' => $temperature->getId()]);
        if (\is_null($temperature)) {
            return;
        }

        return $environment->render(
            '@MiuzeTemperature/Twig/OneWire/temperature.html.twig',
            array(
                'temperature' => $temperature
            )
        );
    }

}
