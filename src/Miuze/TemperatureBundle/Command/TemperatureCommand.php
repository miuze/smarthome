<?php

namespace Miuze\TemperatureBundle\Command;

//use Miuze\TemperatureBundle\Service\Logger;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class TemperatureCommand extends ContainerAwareCommand
{
    private $oneWirePath = __DIR__ . '/../../../../var/1wire';
    protected function configure()
    {
        $this
            ->setName('miuze:temperature:sync')
            ->setDescription('Update temperature')
            ->addOption('yell', null, InputOption::VALUE_NONE, 'If set, the task will yell in uppercase letters');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $sensors = $doctrine->getRepository('MiuzeTemperatureBundle:Temperature')->findAll();
        $em = $doctrine->getEntityManager();

        $countSensorsToUpdate = 0;
        foreach ($sensors as $temp) {
            $name = $temp->getAddress();
            foreach (scandir($this->oneWirePath) as $file) {
                if ($file != '.' && $file != '..') {
                    $pattern = '/^28.([a-zA-Z0-9]+)/';
                    if ($file == $name) {
                        $toUpdate = shell_exec('cat ' . $this->oneWirePath . '/' . $file . '/temperature');
                        $toUpdate = round($toUpdate, 1);
                        if ($temp->getValue() != $toUpdate) {
                            $temp->setValue($toUpdate);
                            $em->merge($temp);
                            $countSensorsToUpdate++;
                        }
                    }
                }
            }
        }

        $em->flush();
//        if($countSensorsToUpdate >= 1) {
//            $logger = $this->getContainer()->get(Logger::class);
//            $logger->add('temperature', 'success', sprintf('Zaktualizowano temperaturę w %d czujnikach.', $countSensorsToUpdate));
//        }
        return true;
    }
}
