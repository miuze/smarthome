<?php

namespace Miuze\UserBundle\Service;

use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface as Templating;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Miuze\UserBundle\Entity\User;
use Miuze\UserBundle\Exception\UserException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class UserService {
    
    /**
     * @var Doctrine
     */
    protected $doctrine;
        
    /**
     * @var Router
     */
    protected $router;
    
    /**
     * @var Container
     */
    protected $container;
    
    /**
     * @var EncoderFactory
     */
    protected $encoderFactory;
    
    function __construct(Doctrine $doctrine, Router $router, Container $container, EncoderFactory $encoderFactory) {
        $this->doctrine = $doctrine;
        $this->router = $router;
        $this->container = $container;
        $this->encoderFactory = $encoderFactory;
    }
    
    public function addUser(User $user): void
    {
        if(false === \is_null($user->getId())){
            throw new UserException('Taki użytkownik już istnieje');
        }
        $encoder = $this->encoderFactory->getEncoder($user);
        $encodedPasswd = $encoder->encodePassword($user->getPlainPassword(), $user->getSalt());
        $user->setPassword($encodedPasswd);
        $user->setEnabled(true);
        $em = $this->doctrine->getManager();
        $em->persist($user);
        $em->flush();
    }


    public function changePassword(User $user): void
    {
        
        if(null == $user->getPlainPassword()){
            throw new UserException('Nie ustawiono nowego hasła!');
        }

        $encoder = $this->encoderFactory->getEncoder($user);
        $encoderPassword = $encoder->encodePassword($user->getPlainPassword(), $user->getSalt());
        $user->setPassword($encoderPassword);
        $em = $this->doctrine->getManager();
        $em->persist($user);
        $em->flush();
    }

    public function deleteUser(User $user): void
    {
        $admin = $this->doctrine->getRepository('MiuzeUserBundle:User')->getAllAdmin();
        if(count($admin) <= 1){
            throw new UserException('Sysem musi mieć przynajmniej jednego administratora');
        }
        $em = $this->doctrine->getManager();
        $em->remove($user);
        $em->flush();
    }

}
