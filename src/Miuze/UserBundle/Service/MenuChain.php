<?php

declare(strict_types=1);

namespace Miuze\UserBundle\Service;

use Symfony\Component\Yaml\Yaml;
use Miuze\UserBundle\Util\MenuInterface;

class MenuChain
{
    private $menus;

    public function __construct()
    {
        $this->menus = [];
    }

    public function addMenu(MenuInterface $menu): void
    {
        $this->menus[] = $menu;
    }

    public function getMenus():array
    {
        return $this->menus;
    }
}
