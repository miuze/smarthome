<?php


namespace Miuze\UserBundle\EventListeners;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Miuze\UserBundle\Entity\User;

class AuthenticationSuccessListener
{
    protected $ttl;

    public function __construct($ttl)
    {
    $this->ttl = $ttl;
    }

    /**
     * @param AuthenticationSuccessEvent $event
     */
    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {
        $data = $event->getData();
        $user = $event->getUser();
        if (!$user instanceof User) {
            return;
        }
        $data['data'] = array(
            'roles' => $user->getRoles(),
            'firstName' => $user->getFirstName(),
            'lastName' =>$user->getLastName(),
            'phone' => $user->getPhone(),
            'email' => $user->getEmail(),
            'avatar' => $user->getWebPath(),
            'tokenTtl' => $this->ttl,
        );
        $event->setData($data);
    }

}