<?php

namespace Miuze\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="Miuze\UserBundle\Repository\UserRepository")
 * @ORM\Table(name="users")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(fields={"username"})
 * @UniqueEntity(fields={"email"})
 */
class User implements AdvancedUserInterface, \Serializable
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length = 50, unique = true, nullable=true)
     * @Assert\NotBlank
     * @Assert\Length(
     *      min=5,
     *      max=50,
     * )
     */
    private $username;

    /**
     * @ORM\Column(type="string", length = 200, unique = true, nullable = true)
     * @Assert\Email(
     *      groups = {"Manage"}
     * )
     * @Assert\Length(
     *      max = 200,
     *      groups = {"Manage"}
     * )
     */
    private $email;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $alarmPin;

    /**
     * @ORM\Column(type="string", length = 64)
     */
    private $password;

    /**
     * @Assert\NotBlank(
     *      groups = {"ChangePassword"}
     * )
     * @Assert\Length(
     *      min = 6,
     *      groups = {"ChangePassword"}
     * )
     */
    private $plainPassword;

    /**
     * @ORM\Column(name="account_non_expired", type="boolean")
     */
    private $accountNonExpired = true;

    /**
     * @ORM\Column(name="account_non_locked", type="boolean")
     */
    private $accountNonLocked = true;

    /**
     * @ORM\Column(name="credentials_non_expired", type="boolean")
     */
    private $credentialsNonExpired = true;

    /**
     * @ORM\Column(type="boolean")
     */
    private $enabled = false;

    /**
     * @ORM\Column(type="array")
     */
    private $roles;

    /**
     * @ORM\Column(name="action_token", type="string", length = 20, nullable = true)
     */
    private $actionToken;

    /**
     * @ORM\Column(name="register_date", type="datetime")
     */
    private $registerDate;

    /**
     * @Assert\File(
     *      maxSize="12M",
     *      mimeTypes = {"image/jpg", "image/jpeg", "image/png"},
     *      mimeTypesMessage = "Zdjęcie musi być w formacie JPG lub PNG."
     * )
     */
    private $file;

    /**
     * @ORM\Column(type="string", length = 100, nullable = true)
     */
    private $avatar;

    /**
     * @ORM\Column(type="string", length = 100, nullable = true)
     */
    private $path;

    /**
     * @ORM\Column(type="datetime", nullable = true)
     */
    private $updateDate;

    /**
     * @ORM\Column(type="text", length=50, nullable=true)
     * @Assert\NotBlank(
     *      groups = {"Manage"}
     * )
     */
    private $firstName;

    /**
     * @ORM\Column(type="text", length=100, nullable=true)
     * @Assert\NotBlank(
     *     groups = {"Manage"}
     * )
     */
    private $lastName;


    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\Length(
     *      max = 150,
     *      minMessage = "Numer nie może zawierać więcej nież 150 znaków",
     *      groups = {"Register", "Manage"}
     * )
     */
    private $phone;

    private $remember_me;


    function __construct()
    {
        $this->registerDate = new \DateTime();
        $this->roles = array('ROLE_USER');
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set alarmPin
     *
     * @param string $alarmPin
     *
     * @return User
     */
    public function setAlarmPin($alarmPin)
    {
        $this->alarmPin = $alarmPin;

        return $this;
    }

    /**
     * Get alarmPin
     *
     * @return string
     */
    public function getAlarmPin()
    {
        return $this->alarmPin;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set registerDate
     *
     * @param \DateTime $registerDate
     *
     * @return User
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Get registerDate
     *
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     *
     * @return User
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return User
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set updateDate
     *
     * @param \DateTime $updateDate
     *
     * @return User
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate
     *
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return mixed
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param mixed $plainPassword
     */
    public function setPlainPassword($plainPassword): void
    {
        $this->plainPassword = $plainPassword;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     */
    public function setFile($file): void
    {
        $this->file = $file;
    }

    /**
     * @return mixed
     */
    public function getRememberMe()
    {
        return $this->remember_me;
    }

    /**
     * @param mixed $remember_me
     */
    public function setRememberMe($remember_me): void
    {
        $this->remember_me = $remember_me;
    }

    /**
     * @return string
     */
    public function serialize()
    {
        return serialize(
            array(
                $this->id,
                $this->username,
                $this->password
            )
        );
    }

    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->username,
            $this->password
            ) = unserialize($serialized);
    }

    public function isAccountNonExpired()
    {
        return $this->accountNonExpired;
    }

    public function isAccountNonLocked()
    {
        return $this->accountNonLocked;
    }

    public function isCredentialsNonExpired()
    {
        return $this->credentialsNonExpired;
    }

    public function isEnabled()
    {
        return $this->enabled;
    }

    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }

    public function getRoles()
    {
        if (empty($this->roles)) {
            return array('ROLE_USER');
        }

        return $this->roles;
    }

    /**
     * Set roles
     *
     * @param array $roles
     * @return User
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }


    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file) {
            $this->setPath('/'.$this->getUploadDir());
            $this->setAvatar(sprintf(
                '%s_%s.%s',
                strtolower($this->getFirstName()),
                strtolower($this->getLastName()),
                $this->file->guessExtension()
             ));
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }
        $this->file->move($this->getUploadRootDir(), $this->avatar);
        $photo =  new \Miuze\UserBundle\Service\ImageService($this->getUploadRootDir() . $this->avatar);
        $photo->resizeToWidth(300);
        $photo->save($this->getUploadRootDir() . $this->avatar);
        unset($this->file);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }

    public function getAbsolutePath()
    {
        return null === $this->avatar ? null : $this->getUploadRootDir().$this->avatar;
    }

    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/avatar/';
    }

    public function getWebPath()
    {
        return null === $this->avatar ? null : $this->getUploadDir() . $this->avatar;
    }

}
