<?php

declare(strict_types = 1);

namespace Miuze\UserBundle\DependencyInjection\Compiler;

use Miuze\UserBundle\Service\MenuChain;
use Miuze\UserBundle\Twig\Extension\RoomExtension;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class ConfigMenuPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if($container->hasDefinition('twig')) {
            $def = $container->getDefinition('twig');
            $def->addMethodCall('addGlobal', array('miuze_user', $container->getParameter('miuze_user')));
        }
        if(!$container->has(MenuChain::class)){
            return;
        }
        $definition = $container->findDefinition(MenuChain::class);
        $taggedServices = $container->findTaggedServiceIds('miuze.user_menu');
        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('addMenu', [new Reference($id)], 1);
        }
    }
}