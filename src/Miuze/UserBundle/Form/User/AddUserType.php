<?php
namespace Miuze\UserBundle\Form\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityRepository;


class AddUserType extends AbstractType{
    
    public function getName() {
        return 'miuze_user_add';
    }    
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        $builder
            ->add('username', TextType::class, array(
                'label' => 'Login *',
                'attr' => array(
                    'placeholder' => 'Login',
                ),
            ))
            ->add('firstName', TextType::class, array(
                'label' => 'Imię *',
                'attr' => array(
                    'placeholder' => 'Imię',
                ),
            ))
            ->add('lastName', TextType::class, array(
                'label' => 'Nazwisko *',
                'attr' => array(
                    'id' => 'blindHeight',
                    'placeholder' => 'Nazwisko',
                ),
            ))
            ->add('email', EmailType::class, array(
                'label' => 'Email',
                'attr' => array(
                    'placeholder' => 'Email',
                ),
            ))
            ->add('phone', TextType::class, array(
                'label' => 'Telefon',
                'attr' => array(
                    'placeholder' => 'Telefon',
                ),
            ))
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'label' => 'Hasło *',
                'first_options' => array(
                    'label' => 'Nowe hasło *'
                ),
                'second_options' => array(
                    'label' => 'Powtórz hasło *'
                )
            ))
            ->add('accountNonExpired', CheckboxType::class, array(
                'label' => 'Konto nie wygasło',
                'required' => FALSE
            ))
            ->add('accountNonLocked', CheckboxType::class, array(
                'label' => 'Konto odblokowane',
                'required' => FALSE
            ))
            ->add('credentialsNonExpired', CheckboxType::class, array(
                'label' => 'Dane uwierzytelniające nie wygasły',
                'required' => FALSE
            ))
            ->add('enabled', CheckboxType::class, array(
                'label' => 'Konto aktywowane',
                'required' => FALSE
            ))
            ->add('roles', ChoiceType::class, array(
                'label' => 'Uprawnienia',
                'multiple' => true,
                'choices' => array(
                    'Użytkownika' => 'ROLE_USER',
                    'Administrator' => 'ROLE_ADMIN',
                )
            ))
            ->add('file', FileType::class, array(
                'label' => 'Avatar',
                'data_class' => NULL
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Dodaj',
                'attr' => array(
                    'class'=> 'btn btn-success'
                )
            ));
    }
    
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\UserBundle\Entity\User',
            'validation_groups' => array('Register')
        ));
    }
}
