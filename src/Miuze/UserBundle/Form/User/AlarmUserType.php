<?php
namespace Miuze\UserBundle\Form\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints as Assert;


class AlarmUserType extends AbstractType{
    
    public function getName() {
        return 'miuze_user_alarm';
    }    
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        $builder
            ->add('alarmPin', PasswordType::class, array(
                'label' => 'Pin',
                'attr' => array(
                    'placeholder' => 'Pin',
                ),
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Dodaj',
                'attr' => array(
                    'class'=> 'btn btn-success'
                )
            ));
    }
    
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\UserBundle\Entity\User'
        ));
    }
}
