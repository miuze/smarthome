<?php

namespace Miuze\UserBundle\Form\Account;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints as Assert;

class AccountSettingsType extends AbstractType {

    public function getName() {
        return 'miuze_user_account_settings';
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('username', TextType::class, array(
                    'label' => 'Login',
                    'required' => FALSE
                ))
                ->add('email', EmailType::class, array(
                    'label' => 'E-mail',
                    'required' => FALSE
                ))
                ->add('firstName', TextType::class, array(
                    'label' => 'Imię',
                    'attr' => array(
                        'placeholder' => 'Imię'
                    )
                ))
                ->add('lastName', TextType::class, array(
                    'label' => 'Nazwisko',
                    'attr' => array(
                        'placeholder' => 'Nazwisko'
                    )
                ))
                ->add('phone', TextType::class, array(
                    'label' => 'Telefon',
                    'attr' => array(
                        'placeholder' => 'Telefon'
                    )
                ))
                ->add('file', FileType::class, array(
                    'label' => 'Avatar',
                    'data_class' => NULL
                ))
                ->add('submit', SubmitType::class, array(
                    'label' => 'Zapisz zmiany'
        ));
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\UserBundle\Entity\User',
            'validation_groups' => array('Manage')
        ));
    }

}
