<?php

namespace Miuze\UserBundle\Form\Login;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class LoginType extends AbstractType
{

    public function getName()
    {
        return 'form_login';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'username',
                TextType::class,
                array(
                    'label' => 'Login',
                    'label_attr' => array(
                        'class' => 'sr-only'
                    ),
                    'attr' => array(
                        'placeholder' => 'Login',
                        'class' => 'form-control'
                    )
                )
            )
            ->add(
                'password',
                PasswordType::class,
                array(
                    'label' => false,
                    'attr' => array(
                        'placeholder' => 'Hasło',
                        'class' => 'form-control'
                    )
                )
            )
            ->add(
                'remember_me',
                CheckboxType::class,
                array(
                    'label' => 'Zapamiętaj mnie',
                    'attr' => array(
                        'class' => 'custom-checkbox-slider',
                        'checked' => true,
                    ),
                )
            )
            ->add(
                'submit',
                SubmitType::class,
                array(
                    'label' => 'Zaloguj',
                )
            );
    }

}