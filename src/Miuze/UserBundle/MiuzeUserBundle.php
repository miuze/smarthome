<?php

declare(strict_types = 1);

namespace Miuze\UserBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Miuze\UserBundle\DependencyInjection\Compiler\ConfigMenuPass;

class MiuzeUserBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new ConfigMenuPass());
    }
}
