<?php

declare(strict_types=1);

namespace Miuze\UserBundle\Twig\Extension;

use Miuze\UserBundle\Util\MenuInterface;
use Miuze\UserBundle\Service\MenuChain;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;

class MenuExtension extends \Twig_Extension
{
    /**
     * @var MenuChain
     */
    private $menuService;

    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(MenuChain $menuChain, ContainerInterface $container)
    {
        $this->menuService = $menuChain;
        $this->container = $container;
    }


    public function getName()
    {
        return 'miuze_user_menu_extension';
    }

    public function getFunctions(): array
    {
        return array(
            new \Twig_SimpleFunction(
                'render_menu', [$this, 'render'],
                [
                    'is_safe' => ['html'],
                    'needs_environment' => true,
                ]
            ),
        );
    }

    public function render(\Twig_Environment $environment)
    {
        $arrayMenu = [];
        foreach ($this->menuService->getMenus() as $menu) {
            $menuService = $this->container->get(get_class($menu));
            foreach ($menuService->generate() as $item)
                array_push($arrayMenu, $item);
        }

        return $environment->render(
            '@MiuzeUser/Twig/Menu/menu.html.twig',
            [
                'menu' => $arrayMenu,
            ]
        );
    }



}
