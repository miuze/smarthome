<?php

namespace Miuze\UserBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Miuze\UserBundle\Exception\UserException;
use Miuze\UserBundle\Entity\User;
use Miuze\UserBundle\Form\Account\AccountSettingsType;
use Miuze\UserBundle\Form\Account\ChangePasswordType;

/**
 * @Route(
 *      "/account",
 * )
 */
class AccountController extends DefaultController
{
    
    /**
     * @Route(
     *     "/settings",
     *      name="miuze_user_account_settings"
     * )
     */
    public function settingsAction(Request $request)
    {
        $entity = $this->getUser();
        $form = $this->createForm(AccountSettingsType::class, $entity);
        if($request->isMethod('POST')){
            $form->handleRequest($request);
            if($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                $this->setMessage('success', 'Konto zostało zaktualizowane');

                return $this->redirect($this->generateUrl('index'));
            }
        }

        return $this->view(
            '@MiuzeUser/Account/settings.html.twig',
            [
                'action' => 'Ustawienia',
                'form' => $form->createView(),
            ]
        );

    }
    
    /**
     * @Route(
     *      "/change-password",
     *      name = "miuze_user_account_change-password",
     * )
     */
    public function changepassAction(Request $request)
    {        
        $entity = $this->getUser();
        if($entity == null){
            $this->get('session')->getFlashBag()->add('danger', 'Brak użytkownika!');

            return $this->redirect($this->generateUrl('index'));
        }
        $form = $this->createForm( ChangePasswordType::class, $entity);
        if($request->isMethod(Request::METHOD_POST)){
            $form->handleRequest($request);
            if($form->isValid()){
                try {
                    $userManager = $this->get('user_manager');
                    $userManager->changePassword($entity);
                    $this->setMessage('success', 'Twoje hasło zostało zmienione!');

                    return $this->redirect($this->generateUrl('index'));
                    
                } catch (UserException $ex) {
                    $this->setMessage('error', $ex->getMessage());
                }
                
            }else{
                $this->setMessage('error', 'Popraw błędy formularza2!');
            }
        }

        return $this->view(
            '@MiuzeUser/Account/changepass.html.twig',
            [
                'action' => 'Zmiana hasła',
                'form' => $form->createView(),
            ]
        );
    }
}
