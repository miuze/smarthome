<?php

declare(strict_types=1);

namespace Miuze\UserBundle\Controller;

use Miuze\UserBundle\Form\Login\LoginType;
use Symfony\Component\Routing\Annotation\Route;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class LoginController extends DefaultController
{
    /**
     * @Route(
     *     "/login",
     *     name = "miuze_user_login"
     *     )
     */
    public function loginAction(AuthenticationUtils $authenticationUtils)
    {
        return $this->view(
            '@MiuzeUser/Login/login.html.twig',
            [
                'action' => 'Zaloguj się',
                'error' => $authenticationUtils->getLastAuthenticationError(),
                'form' => $this->createForm(
                    LoginType::class,
                    array(
                        'username' => $authenticationUtils->getLastUsername(),
                    ))->createView(),
            ]
        );
    }

}
