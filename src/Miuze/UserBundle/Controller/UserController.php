<?php

declare(strict_types=1);

namespace Miuze\UserBundle\Controller;

use Miuze\UserBundle\Service\MenuLoadService;
use Miuze\UserBundle\Util\MenuInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

use Miuze\UserBundle\Form\User\AddUserType;
use Miuze\UserBundle\Form\User\ChangePasswordAdminType;
use Miuze\UserBundle\Form\User\ManageUserType;
use Miuze\UserBundle\Entity\User;
use Miuze\UserBundle\Exception\UserException;
use Miuze\UserBundle\Form\User\AlarmUserType;
use Miuze\UserBundle\Service\UserService;

/**
 * @Route("/users")
 */
class UserController extends DefaultController
{

    /**
     * @Route(
     *      "/",
     *      name = "miuze_user_user_index"
     * )
     */
    public function indexAction()
    {
        $list = $this->getDoctrine()->getManager()->getRepository('MiuzeUserBundle:User')->findAll();

        return $this->view(
            '@MiuzeUser/User/index.html.twig',
            [
                'action' => 'Lista użytkowników',
                'list' => $list,
            ]
        );
    }
    
    /**
     * @Route(
     *      "/change-password/{id}",
     *      name = "miuze_user_user_change-password",
     * )
     */
    public function changepassAction(Request $request, $id)
    {        
        $entity = $this->getDoctrine()->getManager()->getRepository('MiuzeUserBundle:User')->findOneBy(array('id' => $id));
        if(\is_null($entity)){
            $this->setMessage('danger', 'Brak użytkownika o takich parametrach!');

            return $this->redirect($this->generateUrl('user_user_index'));
        }
        $form = $this->createForm( ChangePasswordAdminType::class, $entity);
        if($request->isMethod(Request::METHOD_POST)){
            $form->handleRequest($request);
            if($form->isValid()){
                try {
                    $userManager = $this->get('Miuze\UserBundle\Service\UserService');
                    $userManager->changePassword($entity);
                    $this->setMessage('success', 'Twoje hasło zostało zmienione!');

                    return $this->redirect($this->generateUrl('miuze_user_user_index'));
                } catch (UserException $ex) {
                    $this->setMessage('error', $ex->getMessage());
                }
            }else{
                $this->setMessage('error', 'Popraw błędy formularza2!');
            }
        }

        return $this->view(
            '@MiuzeUser/User/changepass.html.twig',
            [
                'action'=> 'Zmień hasło',
                'form' => $form->createView(),
            ]
        );
    }


    /**
     * @Route(
     *      "/add/{id}",
     *      name = "miuze_user_user_add",
     *      defaults={"id": "0"},
     * )
     */
    public function addUserAction(Request $request, $id)
    {
        $user = new User();
        $form = $this->createForm( AddUserType::class, $user);
        if($request->isMethod(Request::METHOD_POST)){
            $form->handleRequest($request);
            if($form->isValid()){
                try{
                    $userManager = $this->get(UserService::class);
                    $userManager->addUser($user);
                    $this->setMessage('success', 'Konto zostało utworzone.');
                    
                    return $this->redirect($this->generateUrl('miuze_user_user_index'));
                } catch (UserException $e) {
                    $this->setMessage('error', $ex->getMessage());
                }
            }else{
                $this->get('session')->getFlashBag()->add('error', 'Sprawdź błędy w formularzu');
            }
        }

        return $this->view(
            '@MiuzeUser/User/addUser.html.twig',
            [
                'action'=> 'Dodaj użytkownika',
                'form' => $form->createView(),
            ]
        );
    }
    
    
    /**
     * @Route(
     *      "/edit/{id}", 
     *      name="miuze_user_user_manage",
     *      requirements={"id"="\d+"}
     * )
     */
    public function manageAction(Request $request)
    {
        $entity = $this->getDoctrine()->getRepository('MiuzeUserBundle:User')->findOneById($request->attributes->getInt('id'));
        if(is_null($entity)){
            $this->setMessage('danger', 'Nie znaleziono takiego użytkownika');
        }
        $form = $this->createForm( ManageUserType::class, $entity);
        $form->handleRequest($request);
        if($form->isValid()){
            $users = $this->getDoctrine()->getRepository('MiuzeUserBundle:User')->getAllAdmin();
            if(count($users) <= 1 && !in_array('ROLE_ADMIN', $entity->getRoles())){
                $this->setMessage('warning', 'To już ostatni admin. Nie można wykonać operacji');

                return $this->redirect($this->generateUrl('user_user_manage', array('id' => $entity->getId())));
            }
            $em = $this->getDoctrine()->getManager();
            $entity->preUpload();
            $em->persist($entity);
            $em->flush();
            $this->setMessage('success', 'Zaktualizowano konto użytkownika');

            return $this->redirect($this->generateUrl('miuze_user_user_index'));
        }

        return $this->view(
            '@MiuzeUser/User/manage.html.twig',
            [
                'action'=> 'Zarządzaj',
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route(
     *      "/delete/{id}", 
     *      name="miuze_user_user_delete",
     *      requirements={"id"="\d+"}
     * )
     */
    public function deleteAction(Request $request)
    {
        $id = $request->attributes->getInt('id');
        $user = $this->getDoctrine()->getRepository('MiuzeUserBundle:User')->findOneById($id);
        $userManager = $this->get(UserService::class);
        try {
            $userManager->deleteUser($user);
            $this->setMessage('success', 'Usunięto użytkowniak');
        } catch(\UserException $e) {
            $this->setMessage('error', $e->getMessage());
        }

        return $this->redirect($this->generateUrl('miuze_user_user_index'));
    }
    
    /**
     * @Route(
     *      "/alarm/{id}", 
     *      name="miuze_user_user_alarm",
     * )
     */
    public function alarmAction(Request $request, User $user) {
        $form = $this->createForm(AlarmUserType::class, $user);
        $form->handleRequest($request);
        if($form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $user->preUpload();
            $em->persist($user);
            $em->flush();
            $this->setMessage('success', 'Skonfigurowano alarm');

            return $this->redirect($this->generateUrl('miuze_user_user_index'));
        }

        return $this->view(
            '@MiuzeUser/User/alarm.html.twig',
            [
                'action'=> 'Alarm',
                'form' => $form->createView(),
            ]
        );
    }
}
