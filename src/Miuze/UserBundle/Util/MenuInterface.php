<?php

declare(strict_types=1);

namespace Miuze\UserBundle\Util;

interface MenuInterface
{
    public function generate();
}