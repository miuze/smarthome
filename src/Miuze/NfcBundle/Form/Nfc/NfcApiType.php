<?php

declare(strict_types=1);

namespace Miuze\NfcBundle\Form\Nfc;

use Miuze\NfcBundle\Entity\Nfc;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class NfcApiType extends AbstractType {

    public function getName() {
        return 'miuze_nfc_nfc-api';
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('code', TextType::class, array(
                'label' => 'Kod nfc',
                'required' => true,
                'attr' => array(
                    'placeholder' => 'Kod nfc',
                )
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz zmiany'
        ));
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => Nfc::class,
        ));
    }

}
