<?php

namespace Miuze\NfcBundle\Form\Nfc;

use Miuze\NfcBundle\Entity\ActionMarker;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class MarkerNfcType extends AbstractType {

    public function getName() {
        return 'miuze_nfc_nfc-marker';
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('marker', EntityType::class, array(
                'label' => 'Marker',
                'attr' => array(
                    'class' => 'select-input',
                ),
                'class' => 'MiuzePlcBundle:ButtonMarker',
                'choice_label' => function($marker){
                    return $marker->getMarker()->getName() . ' - '. $marker->getMarker()->getAddress();
                },
            ))
            ->add('value', EntityType::class, array(
                'label' => 'Wartość',
                'class' => 'MiuzeNfcBundle:MarkerValue',
                'choice_label' => function($value){
                    return $value->getName();
                },
            ))
            ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => ActionMarker::class,
            'csrf_protection' => false,
        ));
    }

}
