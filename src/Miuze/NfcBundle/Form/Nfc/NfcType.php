<?php

declare(strict_types=1);

namespace Miuze\NfcBundle\Form\Nfc;

use Miuze\NfcBundle\Entity\Nfc;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class NfcType extends AbstractType {

    public function getName() {
        return 'miuze_nfc_nfc';
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Nazwa',
                'required' => true,
                'attr' => array(
                    'placeholder' => 'Nazwa',
                )
            ))
            ->add('code', TextType::class, array(
                'label' => 'Kod nfc',
                'required' => true,
                'disabled' => true,
                'attr' => array(
                    'placeholder' => 'Kod nfc',
                )
            ))
            ->add('actions', CollectionType::class, array(
                'label' => false,
                'allow_add' => true,
                'entry_type' => \Miuze\NfcBundle\Form\Nfc\MarkerNfcType::class,
                'by_reference' => false,
                'allow_delete' =>true,
                'entry_options' => [
                    'attr' => ['class' => 'buttonMarker-box'],
                ],
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz zmiany'
        ));
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => Nfc::class,
            'csrf_protection' => false,
        ));
    }

}
