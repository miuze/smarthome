<?php

declare(strict_types=1);

namespace Miuze\NfcBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
* @ORM\Table(name="nfc_marker_value")
*/
class MarkerValue {

    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(
     *      targetEntity = "Miuze\NfcBundle\Entity\ActionMarker",
     *      mappedBy = "value",
     *      cascade={"all"}
     * )
     */
    protected $marker;

    /**
     * @ORM\Column(type="boolean")
     * @Assert\NotBlank()
     */
    private $value = 1;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     */
    private $name;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->marker = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value.
     *
     * @param bool $value
     *
     * @return MarkerValue
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value.
     *
     * @return bool
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return MarkerValue
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add marker.
     *
     * @param \Miuze\NfcBundle\Entity\ActionMarker $marker
     *
     * @return MarkerValue
     */
    public function addMarker(\Miuze\NfcBundle\Entity\ActionMarker $marker)
    {
        $this->marker[] = $marker;

        return $this;
    }

    /**
     * Remove marker.
     *
     * @param \Miuze\NfcBundle\Entity\ActionMarker $marker
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeMarker(\Miuze\NfcBundle\Entity\ActionMarker $marker)
    {
        return $this->marker->removeElement($marker);
    }

    /**
     * Get marker.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMarker()
    {
        return $this->marker;
    }
}
