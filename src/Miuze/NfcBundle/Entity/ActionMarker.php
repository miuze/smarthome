<?php

declare(strict_types=1);

namespace Miuze\NfcBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
* @ORM\Table(name="nfc_markers")
*/
class ActionMarker {

    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Nfc",
     *      inversedBy = "actions"
     * )
     * @ORM\JoinColumn(
     *      name = "nfc_id",
     *      referencedColumnName= "id",
     *      onDelete = "CASCADE"
     * )
     */
    private $nfc;
    
    
    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Miuze\PlcBundle\Entity\ButtonMarker",
     *      inversedBy = "nfc"
     * )
     * @ORM\JoinColumn(
     *      name = "nfc_marker_id",
     *      referencedColumnName= "id",
     *      onDelete = "CASCADE"
     * )
     */
    private $marker;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "MarkerValue",
     *      inversedBy = "marker"
     * )
     * @ORM\JoinColumn(
     *      name = "marker_value_id",
     *      referencedColumnName= "id",
     *      onDelete = "CASCADE"
     * )
     */
    private $value;



    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nfc.
     *
     * @param \Miuze\NfcBundle\Entity\Nfc|null $nfc
     *
     * @return ActionMarker
     */
    public function setNfc(\Miuze\NfcBundle\Entity\Nfc $nfc = null)
    {
        $this->nfc = $nfc;

        return $this;
    }

    /**
     * Get nfc.
     *
     * @return \Miuze\NfcBundle\Entity\Nfc|null
     */
    public function getNfc()
    {
        return $this->nfc;
    }

    /**
     * Set marker.
     *
     * @param \Miuze\PlcBundle\Entity\ButtonMarker|null $marker
     *
     * @return ActionMarker
     */
    public function setMarker(\Miuze\PlcBundle\Entity\ButtonMarker $marker = null)
    {
        $this->marker = $marker;

        return $this;
    }

    /**
     * Get marker.
     *
     * @return \Miuze\PlcBundle\Entity\ButtonMarker|null
     */
    public function getMarker()
    {
        return $this->marker;
    }

    /**
     * Set value.
     *
     * @param \Miuze\NfcBundle\Entity\MarkerValue|null $value
     *
     * @return ActionMarker
     */
    public function setValue(\Miuze\NfcBundle\Entity\MarkerValue $value = null)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value.
     *
     * @return \Miuze\NfcBundle\Entity\MarkerValue|null
     */
    public function getValue()
    {
        return $this->value;
    }
}
