<?php

declare(strict_types=1);

namespace Miuze\NfcBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Swagger\Annotations as SWG;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Miuze\NfcBundle\Repository\NfcRepository")
 * @ORM\Table(name="nfc")
 * @UniqueEntity(fields={"code"})
 */
class Nfc
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @SWG\Property(type="int", description="Nfc id")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     * @SWG\Property(type="string", description="Nazwa")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=500, unique = true)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=500
     * )
     * @SWG\Property(type="string", description="Kod nfc")
     */
    private $code;

    /**
     * @ORM\OneToMany(
     *      targetEntity = "ActionMarker",
     *      mappedBy = "nfc",
     *      cascade={"all"},
     *      orphanRemoval=true
     * )
     */
    protected $actions;

    public function __construct()
    {
        $date = new \DateTime('now');
        $this->setName(sprintf('Tag from day: %s', $date->format('Y-m-d H:i:s')));
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Nfc
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code.
     *
     * @param string $code
     *
     * @return Nfc
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Add action.
     *
     * @param \Miuze\NfcBundle\Entity\ActionMarker $action
     *
     * @return Nfc
     */
    public function addAction(\Miuze\NfcBundle\Entity\ActionMarker $action)
    {
        $this->actions[] = $action;
        $action->setNfc($this);

        return $this;
    }

    /**
     * Remove action.
     *
     * @param \Miuze\NfcBundle\Entity\ActionMarker $action
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeAction(\Miuze\NfcBundle\Entity\ActionMarker $action)
    {
        return $this->actions->removeElement($action);
    }

    /**
     * Get actions.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActions()
    {
        return $this->actions;
    }
}
