<?php

namespace Miuze\NfcBundle\Controller;

use Miuze\NfcBundle\Entity\Nfc;
use Miuze\NfcBundle\Form\Nfc\NfcApiType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use Symfony\Component\HttpFoundation\Response;
use Swagger\Annotations as SWG;
use Miuze\SettingsBundle\Entity\Room;

/**
 * @Route("/api")
 */
class NfcApiController extends FOSRestController
{
    /**
     * @Post("/nfc", name="_api_nfc_add")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Okiekt dodany poprawnie"
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Błąd parametrów",
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Blędna autoryzacja",
     * )
     * @SWG\Parameter(
     *     name="payload",
     *     in="body",
     *     required=true,
     *     type="json",
     *     format="application/json",
     *     description="Podaj odczytany kod z tagu nfc w formacie json. System doda go do bazy danych.",
     *     @SWG\Schema(
     *          @SWG\Property(
     *              property="code",
     *              type="string",
     *              example="asdj9a8asyu98w20j00q9"
     *          )
     *      ),
     * )
     * @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     required=true,
     *     type="string",
     *     description="Token z logowania",
     * )
     * @SWG\Tag(name="Nfc")
     */
    public function postAction(Request $request)
    {
        if(is_null($request->get('code'))){
            return new JsonResponse('Brak parametru', Response::HTTP_BAD_REQUEST);
        }

        $exist = $this->getDoctrine()->getRepository('MiuzeNfcBundle:Nfc')->findOneByCode($request->get('code'));
        if(is_null($exist) === false){
            if(is_null($request->get('code'))){
                return new JsonResponse('Taki tag już istnieje', Response::HTTP_BAD_REQUEST);
            }
        }
        $data = $request->getContent();
        $data = json_decode($data, 1);

        $entity = new Nfc();
        $form = $this->createForm(NfcApiType::class, $entity);
        $form->submit($data);
        try {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return new JsonResponse('Poprawnie dodano tag');
        } catch (\Exception $e) {

            return new JsonResponse($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Put("/nfc", name="_api_nfc_change")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Okiekt dodany poprawnie",
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Błąd parametrów",
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Blędna autoryzacja",
     * )
     * @SWG\Parameter(
     *     name="payload",
     *     in="body",
     *     required=true,
     *     type="json",
     *     format="application/json",
     *     description="Podaj odczytany kod z tagu nfc w formacie json. Uruchomi akcje przypisane do tagu.",
     *     @SWG\Schema(
     *          @SWG\Property(
     *              property="code",
     *              type="string",
     *              example="asdj9a8asyu98w20j00q9"
     *          )
     *      ),
     * )
     * @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     required=true,
     *     type="string",
     *     description="Token z logowania",
     * )
     * @SWG\Tag(name="Nfc")
     */
    public function putAction(Request $request)
    {
        if(is_null($request->get('code'))){
            return new JsonResponse('Brak parametru', Response::HTTP_BAD_REQUEST);
        }

        $entity = $this->getDoctrine()->getRepository('MiuzeNfcBundle:Nfc')->getNfcByCode($request->get('code'));
        if(is_null($entity)){
            if(is_null($request->get('code'))){
                return new JsonResponse('Taki tag nie istnieje', Response::HTTP_BAD_REQUEST);
            }
        }
        $plcService = $this->get("Miuze\PlcBundle\Service\Modbus");
        try {
            foreach ($entity->getActions() as $marker) {
                $plcService->sendStatusMarkerToPlc(
                    $marker->getMarker()->getMarker(),
                    $marker->getValue()->getValue()
                );
            }
            return new JsonResponse(
                [
                    sprintf('Akcja %s uruchomiona', $entity->getName()),
                ]
            );
        } catch (\Exception $e){
            return new JsonResponse(
                [
                    $e->getMessage(),
                ]
            );
        }
    }
}
