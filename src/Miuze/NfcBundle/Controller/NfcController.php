<?php

namespace Miuze\NfcBundle\Controller;

use Miuze\NfcBundle\Entity\Nfc;
use Miuze\NfcBundle\Form\Nfc\NfcApiType;
use Miuze\NfcBundle\Form\Nfc\NfcType;
use Miuze\RoomBundle\Controller\DefaultController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
* @Route("/admin/nfc")
*/
class NfcController extends DefaultController
{
    public $defaultRoutePlc = 'miuze_nfc_nfc_index';

    /**
     * @Route(
     *      "/",
     *      name="miuze_nfc_nfc_index"
     * )
     */
    public function indexAction()
    {
        $list = $this->getDoctrine()->getRepository('MiuzeNfcBundle:Nfc')->findAll();

        return $this->view(
            '@MiuzeNfc/Nfc/index.html.twig',
            [
                'action' => 'Lista tagów',
                'list' => $list,
            ]
        );
    }


    /**
     * @Route(
     *      "/edit/{id}",
     *      name="miuze_nfc_nfc_edit"
     * )
     */
    public function editAction(Request $request){

        $entity = $this->getDoctrine()
            ->getRepository('MiuzeNfcBundle:Nfc')
            ->findOneById($request->attributes->getInt('id'));
        if($entity == null){
            $this->setMessage('danger', 'Nie znaleziono tagu');
            return $this->redirect($this->generateUrl($this->defaultRoutePlc));
        }

        $form = $this->createForm(NfcType::class, $entity);
        if($request->isMethod(Request::METHOD_POST)){
            $form->handleRequest($request);
            if($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $em->merge($entity);
                $em->flush();
                $this->setMessage('success', 'Zapisano ustawienia');

                return $this->redirect($this->generateUrl($this->defaultRoutePlc));
            }
        }

        return $this->view(
            '@MiuzeNfc/Nfc/add.html.twig',
            [
                'action' => 'Edycja tagu',
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route(
     *      "/delete/{id}",
     *      name="miuze_nfc_nfc_delete"
     * )
     */
    public function deleteAction(Request $request){

        $entity = $this->getDoctrine()
            ->getRepository('MiuzeNfcBundle:Nfc')
            ->findOneById($request->attributes->getInt('id'));
        if($entity == null){
            $this->setMessage('danger', 'Nie znaleziono tagu');
            
            return $this->redirect($this->generateUrl($this->defaultRoutePlc));
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();
        $this->setMessage('success', 'Tag usunięty');

        return $this->redirect($this->generateUrl($this->defaultRoutePlc));
    }
}
