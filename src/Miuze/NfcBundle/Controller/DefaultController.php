<?php

namespace Miuze\NfcBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    private $module = 'Tagi nfc';

    public function view(string $template, array $params = [])
    {
        $params = array_merge(
            $params,
            [
                'module' => $this->module,
            ]
        );
        $view = $this->renderView($template, $params);
        return new Response($view);
    }

    protected function setMessage(string $type, string $msg): void
    {
        $this->get('session')->getFlashBag()->add($type, $msg);
    }
}
