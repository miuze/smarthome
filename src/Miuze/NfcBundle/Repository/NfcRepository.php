<?php

declare(strict_types=1);

namespace Miuze\NfcBundle\Repository;

use Doctrine\ORM\EntityRepository;

class NfcRepository extends EntityRepository
{
    public function getNfcByCode($code)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('n, am, mv, m')
            ->from('MiuzeNfcBundle:Nfc', 'n')
            ->leftJoin('n.actions', 'am')
            ->leftJoin('am.marker', 'm')
            ->leftJoin('am.value', 'mv')
            ->where('n.code = :code')
            ->setParameter('code', $code);
        $result = $qb->getQuery()->getResult();

        return end($result);
    }

}