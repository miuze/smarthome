<?php

declare(strict_types=1);

namespace Miuze\RoomBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Miuze\RoomBundle\Repository\RoomRepository")
 * @ORM\Table(name="rooms")
 * @ORM\HasLifecycleCallbacks
 */
class Room
{
    private $em;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @SWG\Property(description="The unique identifier of the room")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     * @SWG\Property(type="string", maxLength=255, description="Name of room.")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @SWG\Property(type="string", maxLength=255, description="Slug of room.")
     * @Gedmo\Slug(fields={"name", "id"}, separator="-", unique=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     * @SWG\Property(type="string", maxLength=255, description="Class name of font awesome")
     */
    private $icon;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @SWG\Property(type="string", maxLength=255, description="Cords in home image")
     */
    private $cords;

    /**
     * @ORM\Column(type="integer", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     * @SWG\Property(type="integer", description="Number od sort room")
     */
    private $sort = 1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @SWG\Property(type="string", maxLength=255, description="Path of room image.")
     */
    private $path;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @SWG\Property(type="string", maxLength=255, description="Name of room image.")
     */
    private $image;

    /**
     * @Assert\File(
     *      maxSize="3M",
     *      mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png", "application/pdf"},
     *      mimeTypesMessage = "Zdjęcie musi być w formacie JPG, GIF ou PNG a plik w PDF"
     * )
     */
    private $file;

    /**
     * @ORM\OneToMany(
     *      targetEntity = "View",
     *      mappedBy = "room"
     * )
     * @SWG\Property(@Model(type=View::class))
     */
    private $views;

    /**
     * @ORM\Column(type="boolean")
     */
    private $main = 0;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->views = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Room
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set icon
     *
     * @param string $icon
     *
     * @return Room
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set cords
     *
     * @param string $cords
     *
     * @return Room
     */
    public function setCords($cords)
    {
        $this->cords = $cords;

        return $this;
    }

    /**
     * Get cords
     *
     * @return string
     */
    public function getCords()
    {
        return $this->cords;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return Room
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Room
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Add view
     *
     * @param \Miuze\RoomBundle\Entity\View $view
     *
     * @return Room
     */
    public function addView(\Miuze\RoomBundle\Entity\View $view)
    {
        $this->views[] = $view;

        return $this;
    }

    /**
     * Remove view
     *
     * @param \Miuze\RoomBundle\Entity\View $view
     */
    public function removeView(\Miuze\RoomBundle\Entity\View $view)
    {
        $this->views->removeElement($view);
    }

    /**
     * Get views
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     */
    public function setFile($file): void
    {
        $this->file = $file;
    }

    /**
     * Set sort
     *
     * @param integer $sort
     *
     * @return Room
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * Get sort
     *
     * @return integer
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * Set main
     *
     * @param boolean $main
     *
     * @return Room
     */
    public function setMain($main)
    {
        $this->main = $main;

        return $this;
    }

    /**
     * Get main
     *
     * @return boolean
     */
    public function getMain()
    {
        return $this->main;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug): void
    {
        $this->slug = $slug;
    }

    public function getAbsolutePath()
    {
        return null === $this->image ? null : $this->getUploadRootDir().'/'.$this->image;
    }

    public function getWebPath()
    {
        return null === $this->image ? null : $this->getUploadDir().'/'.$this->image;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/room';
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file) {
            $this->setPath('/uploads/room/');
            $this->setImage(uniqid().'.'.$this->file->guessExtension());
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }
        $this->file->move($this->getUploadRootDir(), $this->image);
        unset($this->file);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }
}
