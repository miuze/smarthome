<?php

declare(strict_types=1);

namespace Miuze\RoomBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Swagger\Annotations as SWG;

/**
 * @ORM\Entity(repositoryClass="Miuze\RoomBundle\Repository\ViewRepository")
 * @ORM\Table(name="views")
 * @ORM\HasLifecycleCallbacks
 */
class View
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     */
    private $name;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Room",
     *      inversedBy = "views"
     * )
     * @ORM\JoinColumn(
     *      name = "room_id",
     *      referencedColumnName= "id",
     *      onDelete = "CASCADE"
     * )
     */
    private $room;

    /**
     * @ORM\OneToMany(
     *      targetEntity = "ViewItem",
     *      mappedBy = "view",
     *      cascade={"persist", "merge", "remove"},
     *      orphanRemoval=true
     * )
     */
    protected $items;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     */
    private $sort = 1;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->items = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return View
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set room
     *
     * @param \Miuze\RoomBundle\Entity\Room $room
     *
     * @return View
     */
    public function setRoom(\Miuze\RoomBundle\Entity\Room $room = null)
    {
        $this->room = $room;

        return $this;
    }

    /**
     * Get room
     *
     * @return \Miuze\RoomBundle\Entity\Room
     */
    public function getRoom()
    {
        return $this->room;
    }


    /**
     * Add item
     *
     * @param \Miuze\RoomBundle\Entity\ViewItem $item
     *
     * @return View
     */
    public function addItem(\Miuze\RoomBundle\Entity\ViewItem $item)
    {
        $item->setView($this);
        $this->items[] = $item;

        return $this;
    }

    /**
     * Remove item
     *
     * @param \Miuze\RoomBundle\Entity\ViewItem $item
     */
    public function removeItem(\Miuze\RoomBundle\Entity\ViewItem $item)
    {
        $this->items->removeElement($item);
    }

    /**
     * Get items
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Set sort
     *
     * @param integer $sort
     *
     * @return View
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * Get sort
     *
     * @return integer
     */
    public function getSort()
    {
        return $this->sort;
    }
}
