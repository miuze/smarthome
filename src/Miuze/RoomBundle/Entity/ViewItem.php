<?php

declare(strict_types=1);

namespace Miuze\RoomBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Miuze\RoomBundle\Repository\ViewItemRepository")
 * @ORM\Table(name="buttons_to_views")
 * @ORM\HasLifecycleCallbacks
 */
class ViewItem
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $sort = 1;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "View",
     *      inversedBy = "items",
     *      cascade={"persist", "merge"}
     * )
     * @ORM\JoinColumn(
     *      name = "view_id",
     *      referencedColumnName= "id",
     *      onDelete = "CASCADE"
     * )
     */
    private $view;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Miuze\PlcBundle\Entity\Button",
     *      inversedBy = "view",
     * )
     * @ORM\JoinColumn(
     *      name = "button_id",
     *      referencedColumnName= "id",
     *      onDelete = "CASCADE"
     * )
     */
    private $button;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Miuze\TemperatureBundle\Entity\Temperature",
     *      inversedBy = "view",
     * )
     * @ORM\JoinColumn(
     *      name = "temperature_id",
     *      referencedColumnName= "id",
     *      onDelete = "CASCADE"
     * )
     */
    private $temperature;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Miuze\SceneBundle\Entity\Scene",
     *      inversedBy = "view",
     * )
     * @ORM\JoinColumn(
     *      name = "scene_id",
     *      referencedColumnName= "id",
     *      onDelete = "CASCADE"
     * )
     */
    private $scene;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sort
     *
     * @param integer $sort
     *
     * @return ViewItem
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * Get sort
     *
     * @return integer
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * Set view
     *
     * @param \Miuze\RoomBundle\Entity\View $view
     *
     * @return ViewButtons
     */
    public function setView(\Miuze\RoomBundle\Entity\View $view = null)
    {
        $this->view = $view;

        return $this;
    }

    /**
     * Get view
     *
     * @return \Miuze\RoomBundle\Entity\View
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * Set button
     *
     * @param \Miuze\PlcBundle\Entity\Button $button
     *
     * @return ViewButtons
     */
    public function setButton(\Miuze\PlcBundle\Entity\Button $button = null)
    {
        $this->button = $button;

        return $this;
    }

    /**
     * Get button
     *
     * @return \Miuze\PlcBundle\Entity\Button
     */
    public function getButton()
    {
        return $this->button;
    }

    /**
     * Set buttonPlc
     *
     * @param \Miuze\PlcBundle\Entity\Button $buttonPlc
     *
     * @return ViewButtons
     */
    public function setButtonPlc(\Miuze\PlcBundle\Entity\Button $buttonPlc = null)
    {
        $this->buttonPlc = $buttonPlc;

        return $this;
    }

    /**
     * Get buttonPlc
     *
     * @return \Miuze\PlcBundle\Entity\Button
     */
    public function getButtonPlc()
    {
        return $this->buttonPlc;
    }


    /**
     * Set temperature.
     *
     * @param \Miuze\TemperatureBundle\Entity\Temperature|null $temperature
     *
     * @return ViewItem
     */
    public function setTemperature(\Miuze\TemperatureBundle\Entity\Temperature $temperature = null)
    {
        $this->temperature = $temperature;

        return $this;
    }

    /**
     * Get temperature.
     *
     * @return \Miuze\TemperatureBundle\Entity\Temperature|null
     */
    public function getTemperature()
    {
        return $this->temperature;
    }

    /**
     * Set scene.
     *
     * @param \Miuze\SceneBundle\Entity\Scene|null $scene
     *
     * @return ViewItem
     */
    public function setScene(\Miuze\SceneBundle\Entity\Scene $scene = null)
    {
        $this->scene = $scene;

        return $this;
    }

    /**
     * Get scene.
     *
     * @return \Miuze\SceneBundle\Entity\Scene|null
     */
    public function getScene()
    {
        return $this->scene;
    }
}
