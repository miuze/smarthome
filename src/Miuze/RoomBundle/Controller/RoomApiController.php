<?php

namespace Miuze\RoomBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Get;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Miuze\RoomBundle\Entity\Room;

/**
 * @Route("/api")
 */
class RoomApiController extends FOSRestController
{
    /**
     * @Get("/room/{id}", name="_api_room_get", defaults={"id"=0}, )
     * 
     * @param Request $request
     * @param type $id
     * @return json
     * @SWG\Response(
     *     response=200,
     *     description="Okiekt pomieszczenia lub tablicę obiektów",
     *     @SWG\Schema(ref=@Model(type=Room::class))
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Jeśli obiekt nie istnieje",
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Blędna autoryzacja",
     * )
     * @SWG\Parameter(
     *     name="id",
     *     type="integer",
     *     required=false,
     *     in="query",
     *     description="Podaj jeśli chcesz pobrać jedno pomieszczenie. Bez paramertu otrzymasz tablicę obiektów",
     *     required=false
     * )
     * @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     required=true,
     *     type="string",
     *     description="Token z logowania",
     * )
     * @SWG\Tag(name="Room")
     */
    public function getAction(Request $request)
    {
        $id = $request->attributes->getInt('id');
        if($id !== 0){
            $entity = $this->getDoctrine()
                ->getRepository('MiuzeRoomBundle:Room')
                ->getApiRoomDetails($id);
        }else{
            $entity = $this->getDoctrine()
                ->getRepository('MiuzeRoomBundle:Room')
                ->getApiAllRoom();
        }
        if($entity === false){
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        }

        return new JsonResponse($entity, Response::HTTP_OK);
    }
}
