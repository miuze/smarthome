<?php

namespace Miuze\RoomBundle\Controller;

use Miuze\RoomBundle\Controller\DefaultController;
use Miuze\RoomBundle\Entity\Room;
use Miuze\RoomBundle\Entity\RoomMarker;
use Miuze\RoomBundle\Form\Room\RoomType;
use Miuze\RoomBundle\Form\Room\ViewType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
* @Route("/admin/room")
*/
class RoomController extends DefaultController
{
    public $defaultRoutePlc = 'miuze_room_room_index';

    /**
     * @Route(
     *      "/",
     *      name="miuze_room_room_index"
     * )
     */
    public function indexAction()
    {
        $list = $this->getDoctrine()->getRepository('MiuzeRoomBundle:Room')->findBy([],['sort' => 'ASC']);

        return $this->view(
            '@MiuzeRoom/Room/index.html.twig',
            [
                'action' => 'Lista pomieszczeń',
                'list' => $list,
            ]
        );
    }
    
    /**
     * @Route(
     *      "/add",
     *      name="miuze_room_room_add"
     * )
     */
    public function addAction(Request $request)
    {
        $entity = new Room();
        $form = $this->createForm(RoomType::class, $entity);
        if($request->isMethod(Request::METHOD_POST)){
            $form->handleRequest($request);
            if($form->isValid()){
                $roomManager = $this->get('Miuze\RoomBundle\Service\RoomUpdateManager');
                $roomManager->persistRoom($entity);
                $this->setMessage('success', 'Zapisano ustawienia');

                return $this->redirect($this->generateUrl($this->defaultRoutePlc));
            }
        }

        return $this->view(
            '@MiuzeRoom/Room/add.html.twig',
            [
                'action' => 'Dodaj pomieszczenie',
                'form' => $form->createView(),
            ]
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name="miuze_room_room_edit"
     * )
     */
    public function editAction(Request $request)
    {
        $entity = $this->getDoctrine()
            ->getRepository('MiuzeRoomBundle:Room')
            ->findOneById($request->attributes->getInt('id'));
        if($entity == null){
            $this->setMessage('danger', 'Nie znaleziono pomieszczenia');

            return $this->redirect($this->generateUrl($this->defaultRoutePlc));
        }
        $form = $this->createForm(RoomType::class, $entity);
        if($request->isMethod(Request::METHOD_POST)){
            $form->handleRequest($request);
            if($form->isValid()){
                $roomManager = $this->get('Miuze\RoomBundle\Service\RoomUpdateManager');
                $roomManager->persistRoom($entity);
                $this->setMessage('success', 'Zapisano ustawienia');

                return $this->redirect($this->generateUrl($this->defaultRoutePlc));
            }
        }

        return $this->view(
            '@MiuzeRoom/Room/add.html.twig',
            [
                'action' => 'Edycja pomieszczenia',
                'form' => $form->createView(),
            ]
        );
    }
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="miuze_room_room_delete"
     * )
     */
    public function deleteAction(Request $request)
    {
        $entity = $this->getDoctrine()
            ->getRepository('MiuzeRoomBundle:Room')
            ->findOneById($request->attributes->getInt('id'));
        if($entity == null){
            $this->setMessage('danger', 'Nie znaleziono pomieszczenia');

            return $this->redirect($this->generateUrl($this->defaultRoutePlc));
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();
        $this->setMessage('success', 'Pomieszczenie usunięta');

        return $this->redirect($this->generateUrl($this->defaultRoutePlc));
    }

    /**
     * @Route(
     *      "/favourite/{id}",
     *      name="miuze_room_room_favourite"
     * )
     */
    public function favouriteAction(Request $request)
    {
        $entity = $this->getDoctrine()
            ->getRepository('MiuzeRoomBundle:Room')
            ->findOneById($request->attributes->getInt('id'));
        if($entity == null){
            $this->setMessage('danger', 'Nie znaleziono pomieszczenia');

            return $this->redirect($this->generateUrl($this->defaultRoutePlc));
        }
        $roomManager = $this->get('Miuze\RoomBundle\Service\RoomUpdateManager');
        $entity->setMain(true);
        $roomManager->persistRoom($entity);
        $this->setMessage('success', 'Pomieszczenie zaktualizowane');

        return $this->redirect($this->generateUrl($this->defaultRoutePlc));
    }

    /**
     * @Route(
     *      "/sort",
     *      name="miuze_room_room_sort",
     * )
     */
    public function sortAction(Request $request)
    {
        if ($request->isMethod(Request::METHOD_POST)) {
            $sort = $request->request->get('sort');
            $sort = explode(',', $sort);
            $items = $this->getDoctrine()->getRepository('MiuzeRoomBundle:Room')->getSorts($sort);
            $em = $this->getDoctrine()->getManager();
            foreach($sort as $key => $value){
                foreach($items as $item){
                    if($item->getId() == (int)$value){
                        $item->setSort($key);
                        $em->merge($item);
                    }
                }
            }
            $em->flush();
            $this->setMessage('success', 'Kolejność została zaktualizowana');
        }else {
            $this->setMessage('danger', 'Kolejność nie została zaktualizowana');
        }

        return $this->redirect($request->headers->get('referer'));
    }

}
