<?php

declare(strict_types=1);

namespace Miuze\RoomBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/room")
 */
class FrontController extends Controller
{
    /**
     * @Route(
     *      "/{slug}",
     *      name="miuze_room_front_index",
     *     defaults= {"slug" = 0},
     *     requirements={"slug"="[a-z0-9\-]+"}
     * )
     */
    public function index($slug): Response
    {
        $entity = $this->getDoctrine()->getRepository('MiuzeRoomBundle:Room')->getRoomBySlug($slug);
        if(\is_null($entity)){
            throw new NotFoundHttpException('Nie znaleziono pomieszczenia');
        }
        $view = $this->renderView('@MiuzeRoom/Front/index.html.twig', [
            'room' => $entity
        ]);
        return new Response($view);
    }
}
