<?php

namespace Miuze\RoomBundle\Controller;

use Miuze\RoomBundle\Controller\DefaultController;
use Miuze\RoomBundle\Entity\View;
use Miuze\RoomBundle\Entity\ViewButtons;
use Miuze\RoomBundle\Form\View\ViewType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
* @Route("/admin/room/view")
*/
class ViewController extends DefaultController
{
    public $defaultRoutePlc = 'miuze_room_view_index';

    /**
     * @Route(
     *      "/",
     *      name="miuze_room_view_index"
     * )
     */
    public function indexAction()
    {
        $list = $this->getDoctrine()->getRepository('MiuzeRoomBundle:View')->getWithRoom();

        return $this->view(
            '@MiuzeRoom/View/index.html.twig',
            [
                'action' => 'Lista widoków',
                'list' => $list,
            ]
        );
    }

    /**
     * @Route(
     *      "/room/{id}",
     *      name="miuze_room_view_room"
     * )
     */
    public function roomAction(Request $request, $id)
    {
        $list = $this->getDoctrine()->getRepository('MiuzeRoomBundle:View')->getFromRoom($id);
        if(empty($list)){
            $this->setMessage('danger', 'Nie znaleziono widoków dla pomieszczenia');

            return $this->redirect($request->headers->get('referer'));
        }

        return $this->view(
            '@MiuzeRoom/View/room.html.twig',
            [
                'action' => 'Lista widoków',
                'list' => $list,
            ]
        );
    }

    /**
     * @Route(
     *      "/details/{id}",
     *      name="miuze_room_view_details"
     * )
     */
    public function detailsAction(Request $request, $id)
    {
        $entity = $this->getDoctrine()->getRepository('MiuzeRoomBundle:View')->getViewById($id);
        if(is_null($entity)){
            $this->setMessage('danger', 'Nie znaleziono widoku');

            return $this->redirect($request->headers->get('referer'));
        }
        $list = $this->getDoctrine()->getRepository('MiuzeRoomBundle:ViewItem')->getByViewId($entity->getId());

        return $this->view(
            '@MiuzeRoom/View/details.html.twig',
            [
                'action' => 'Lista przycisków dla widoku: ' . $entity->getName(),
                'entity' => $entity,
                'list' => $list,
            ]
        );
    }
    
    /**
     * @Route(
     *      "/add",
     *      name="miuze_room_view_add"
     * )
     */
    public function addAction(Request $request)
    {
        $entity = new View();
        $form = $this->createForm(ViewType::class, $entity);
        if($request->isMethod(Request::METHOD_POST)){
            $form->handleRequest($request);
            if($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                $this->setMessage('success', 'Zapisano ustawienia');
                if ($form->get('submitAndSort')->isClicked()) {
                    return $this->redirect($this->generateUrl('miuze_room_view_details', ['id' => $entity->getId()]));
                } else {
                    return $this->redirect($this->generateUrl($this->defaultRoutePlc));
                }
            }
        }

        return $this->view(
            '@MiuzeRoom/View/add.html.twig',
            [
                'action' => 'Dodaj widok',
                'form' => $form->createView(),
            ]
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name="miuze_room_view_edit"
     * )
     */
    public function editAction(Request $request)
    {
        $entity = $this->getDoctrine()
            ->getRepository('MiuzeRoomBundle:View')
            ->findOneById($request->attributes->getInt('id'));
        if($entity == null){
            $this->setMessage('danger', 'Nie znaleziono widoku');

            return $this->redirect($this->generateUrl($this->defaultRoutePlc));
        }
        $form = $this->createForm(ViewType::class, $entity);
        if($request->isMethod(Request::METHOD_POST)){
            $form->handleRequest($request);
            if($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $em->merge($entity);
                $em->flush();
                $this->setMessage('success', 'Zapisano ustawienia');

                return $this->redirect($this->generateUrl($this->defaultRoutePlc));
            }
        }

        return $this->view(
            '@MiuzeRoom/View/add.html.twig',
            [
                'action' => 'Edycja widoku',
                'form' => $form->createView(),
            ]
        );
    }
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="miuze_room_view_delete"
     * )
     */
    public function deleteAction(Request $request)
    {
        $entity = $this->getDoctrine()
            ->getRepository('MiuzeRoomBundle:View')
            ->findOneById($request->attributes->getInt('id'));
        if($entity == null){
            $this->setMessage('danger', 'Nie znaleziono widoku');

            return $this->redirect($this->generateUrl($this->defaultRoutePlc));
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();
        $this->setMessage('success', 'Widok usunięty');

        return $this->redirect($this->generateUrl($this->defaultRoutePlc));
    }


    /**
     * @Route(
     *      "/sort",
     *      name="miuze_room_view_sort",
     * )
     */
    public function sortAction(Request $request)
    {
        if ($request->isMethod(Request::METHOD_POST)) {
            $sort = $request->request->get('sort');
            $sort = explode(',', $sort);
            $items = $this->getDoctrine()->getRepository('MiuzeRoomBundle:View')->getSorts($sort);
            $em = $this->getDoctrine()->getManager();
            foreach($sort as $key => $value){
                foreach($items as $item){
                    if($item->getId() == (int)$value){
                        $item->setSort($key);
                        $em->merge($item);
                    }
                }
            }
            $em->flush();
            $this->setMessage('success', 'Kolejność została zaktualizowana');
        }else {
            $this->setMessage('danger', 'Kolejność nie została zaktualizowana');
        }

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Route(
     *      "/sort-button",
     *      name="miuze_room_view_sort-button",
     * )
     */
    public function sortButtonAction(Request $request)
    {
        if ($request->isMethod(Request::METHOD_POST)) {
            $sort = $request->request->get('sort');
            $sort = explode(',', $sort);
            $items = $this->getDoctrine()->getRepository('MiuzeRoomBundle:ViewItem')->getSorts($sort);
            $em = $this->getDoctrine()->getManager();
            foreach($sort as $key => $value){
                foreach($items as $item){
                    if($item->getId() == (int)$value){
                        $item->setSort($key);
                        $em->merge($item);
                    }
                }
            }
            $em->flush();
            $this->setMessage('success', 'Kolejność została zaktualizowana');
        }else {
            $this->setMessage('danger', 'Kolejność nie została zaktualizowana');
        }

        return $this->redirect($request->headers->get('referer'));
    }
}
