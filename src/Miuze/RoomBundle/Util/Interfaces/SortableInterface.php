<?php

declare(strict_types=1);

namespace Miuze\RoomBundle\Util\Interfaces;

interface SortableInterface
{
    public function getSorts(array $ids): ?array;
}