<?php

declare(strict_types=1);

namespace Miuze\RoomBundle\Twig\Extension;

use Doctrine\Bundle\DoctrineBundle\Registry;

class RoomExtension extends \Twig_Extension
{
    private $environment;

    /**
     * @var Registry
     */
    private $doctrine;

    public function __construct(Registry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function getName()
    {
        return 'miuze_room_room_extension';
    }

    public function getFunctions(): array
    {
        return array(
            new \Twig_SimpleFunction(
                'render_rooms', [$this, 'renderRooms'],
                [
                    'is_safe' => ['html'],
                    'needs_environment' => true,
                ]
            ),
        );
    }

    public function renderRooms(\Twig_Environment $environment)
    {
        $list = $this->doctrine->getRepository('MiuzeRoomBundle:Room')->getSortedRooms();

        return $environment->render(
            '@MiuzeRoom/Twig/Room/renderRooms.html.twig',
            [
                'list' => $list,
            ]
        );
    }


}
