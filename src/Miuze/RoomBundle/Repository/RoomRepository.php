<?php

declare(strict_types=1);

namespace Miuze\RoomBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Miuze\RoomBundle\Entity\Room;
use Miuze\RoomBundle\Util\Interfaces\SortableInterface;

class RoomRepository extends EntityRepository implements SortableInterface
{
    public function getRoomById($id): ?Room
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('r, v, items, button')
            ->from('MiuzeRoomBundle:Room', 'r')
            ->leftJoin('r.views', 'v')
            ->leftJoin('v.items', 'items')
            ->leftJoin('items.button', 'button')
            ->where('r.id = :id')
            ->setParameter('id', $id)
        ->orderBy('v.sort, items.sort', 'ASC');
        $result = $qb->getQuery()->getOneOrNullResult();

        return $result ?? $this->getMainRoom();
    }

    public function getRoomBySlug($slug): ?Room
    {
        if($slug === 0){
            return $this->getMainRoom();
        }
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('r, v, items, button')
            ->from('MiuzeRoomBundle:Room', 'r')
            ->leftJoin('r.views', 'v')
            ->leftJoin('v.items', 'items')
            ->leftJoin('items.button', 'button')
            ->where('r.slug = :slug')
            ->setParameter('slug', $slug)
        ->orderBy('v.sort, items.sort', 'ASC');
        $result = $qb->getQuery()->getOneOrNullResult();

        return $result ?? $this->getMainRoom();
    }

    public function getMainRoom(): ?Room
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('r, v, items, button')
            ->from('MiuzeRoomBundle:Room', 'r')
            ->leftJoin('r.views', 'v')
            ->leftJoin('v.items', 'items')
            ->leftJoin('items.button', 'button')
            ->orderBy('v.sort, items.sort', 'ASC')
            ->where('r.main = 1');

        return $result = $qb->getQuery()->getOneOrNullResult();
    }

    public function getToUpdateMain($id): ? array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('r')
            ->from('MiuzeRoomBundle:Room', 'r')
            ->where('r.id <> :id AND r.main = 1')
            ->setParameter('id', $id);

        return $qb->getQuery()->getResult();
    }

    public function getSortedRooms(): ?array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('r')
            ->from('MiuzeRoomBundle:Room', 'r')
            ->orderBy('r.sort', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function getSorts(array $ids): ?array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('r')
            ->from('MiuzeRoomBundle:Room', 'r')
            ->where('r.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->orderBy('r.sort', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function getApiRoomDetails($roomId)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('r, v, item, button, bt, marker, temperature, scene')
            ->from('MiuzeRoomBundle:Room', 'r')
            ->where('r.id = :roomId')
            ->leftJoin('r.views', 'v')
            ->leftJoin('v.items', 'item')
            ->leftJoin('item.button', 'button')
            ->leftJoin('item.temperature', 'temperature')
            ->leftJoin('item.scene', 'scene')
            ->leftJoin('button.buttonMarker', 'bt')
            ->leftJoin('bt.marker', 'marker')
            ->setParameter('roomId', $roomId);

        $result = $qb->getQuery()->getArrayResult();
        return end($result);
    }

    public function getApiAllRoom() {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('r.name, r.id, r.path, r.image')
            ->from('MiuzeRoomBundle:Room', 'r')
            ->orderBy('r.sort', 'ASC');

        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

}
