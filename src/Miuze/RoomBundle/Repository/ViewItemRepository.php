<?php

declare(strict_types=1);

namespace Miuze\RoomBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Miuze\RoomBundle\Entity\View;
use Miuze\RoomBundle\Util\Interfaces\SortableInterface;

class ViewItemRepository extends EntityRepository implements SortableInterface
{

    public function getByViewId(int $id): ?array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('i, button, scene, temperature')
            ->from('MiuzeRoomBundle:ViewItem', 'i')
            ->leftJoin('i.button', 'button')
            ->leftJoin('i.scene', 'scene')
            ->leftJoin('i.temperature', 'temperature')
            ->where('i.view = :id')
            ->setParameter('id', $id)
            ->orderBy('i.sort', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function getSorts(array $ids): ?array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('vb')
            ->from('MiuzeRoomBundle:ViewItem', 'vb')
            ->where('vb.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->orderBy('vb.sort', 'ASC');

        return $qb->getQuery()->getResult();
    }

}
