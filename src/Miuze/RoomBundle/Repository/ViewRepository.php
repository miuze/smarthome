<?php

declare(strict_types=1);

namespace Miuze\RoomBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Miuze\RoomBundle\Entity\View;
use Miuze\RoomBundle\Util\Interfaces\SortableInterface;

class ViewRepository extends EntityRepository implements SortableInterface
{
    public function getViewById(int $id): ?View
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('v, b, r')
            ->from('MiuzeRoomBundle:View', 'v')
            ->leftJoin('v.items', 'b')
            ->leftJoin('v.room', 'r')
            ->where('v.id = :id')
            ->setParameter('id', $id);
        return $qb->getQuery()->getOneOrNullResult();
    }

    public function getWithRoom(): ?array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('v, r, item')
            ->from('MiuzeRoomBundle:View', 'v')
            ->leftJoin('v.room', 'r')
            ->leftJoin('v.items', 'item')
            ->orderBy('v.sort', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function getFromRoom(int $id): ?array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('v')
            ->from('MiuzeRoomBundle:View', 'v')
            ->leftJoin('v.room', 'r')
            ->where('r.id = :id')
            ->setParameter('id', $id, \PDO::PARAM_INT)
            ->orderBy('v.sort', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function getSorts(array $ids): ?array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('v')
            ->from('MiuzeRoomBundle:View', 'v')
            ->where('v.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->orderBy('v.sort', 'ASC');

        return $qb->getQuery()->getResult();
    }

}
