<?php

declare(strict_types=1);

namespace Miuze\RoomBundle\Service;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Miuze\RoomBundle\Entity\Room;

class RoomUpdateManager
{
    /**
     * @var Registry
     */
    private $doctrine;

    public function __construct(Registry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function persistRoom(Room $room){
        $em = $this->doctrine->getManager();
        $em->persist($room);
        $em->flush();
        $rooms = $em->getRepository('MiuzeRoomBundle:Room')->getToUpdateMain($room->getId());
        foreach ($rooms as $room) {
            $room->setMain(false);
            $em->merge($room);
        }
        $em->flush();
    }

}
