<?php

declare(strict_types=1);

namespace Miuze\RoomBundle\Form\View;

use Miuze\RoomBundle\Entity\ViewItem;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ButtonPlcType extends AbstractType {

    public function getName() {
        return 'miuze_room_button';
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('button', EntityType::class, array(
                'label' => 'Przycisk',
                'attr' => array(
                    'class' => 'select-input button',
                ),
                'placeholder' => 'Wybierz',
                'required' => false,
                'class' => 'MiuzePlcBundle:Button',
                'choice_label' => function($button){
                    return sprintf('%s - %s', $button->getName(), $button->getButtonType()->getName());
                },
            ))
            ->add('temperature', EntityType::class, array(
                'label' => 'Czujnik',
                'attr' => array(
                    'class' => 'select-input temperature',
                ),
                'class' => 'MiuzeTemperatureBundle:Temperature',
                'placeholder' => 'Wybierz',
                'required' => false,
                'choice_label' => function($sensor){
                    return $sensor->getName();
                },
            ))
            ->add('scene', EntityType::class, array(
                'label' => 'Scena',
                'attr' => array(
                    'class' => 'select-input scene',
                ),
                'class' => 'MiuzeSceneBundle:Scene',
                'placeholder' => 'Wybierz',
                'required' => false,
                'choice_label' => function($scene){
                    return $scene->getName();
                },
            ));
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => ViewItem::class,
        ));
    }

}
