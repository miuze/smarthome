<?php

declare(strict_types=1);

namespace Miuze\RoomBundle\Form\View;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ViewType extends AbstractType
{

    public function getName()
    {
        return 'miuze_room_view';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                TextType::class,
                array(
                    'label' => 'Nazwa',
                    'required' => true,
                    'attr' => array(
                        'placeholder' => 'Nazwa',
                    )
                )
            )
            ->add(
                'room',
                EntityType::class,
                array(
                    'label' => 'Pomieszczenie',
                    'attr' => array(
                        'class' => 'select-input',
                    ),
                    'class' => 'MiuzeRoomBundle:Room',
                    'choice_label' => function ($room) {
                        return $room->getName();
                    },
                )
            )
            ->add(
                'items',
                CollectionType::class,
                array(
                    'label' => 'Widget',
                    'allow_add' => true,
                    'entry_type' => ButtonPlcType::class,
                    'by_reference' => false,
                    'allow_delete' =>true,
                    'entry_options' => [
                        'attr' => ['class' => 'buttonMarker-box'],
                    ],
                )
            )
            ->add(
                'submit',
                SubmitType::class,
                array(
                    'label' => 'Zapisz zmiany'
                )
            )
            ->add(
                'submitAndSort',
                SubmitType::class,
                array(
                    'label' => 'Zapisz i sortuj'
                )
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Miuze\RoomBundle\Entity\View',
                'csrf_protection' => false,
            )
        );
    }

}
