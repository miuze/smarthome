<?php

declare(strict_types=1);

namespace Miuze\RoomBundle\Form\Room;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class RoomType extends AbstractType {

    public function getName() {
        return 'miuze_room_room';
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Nazwa',
                'required' => true,
                'attr' => array(
                    'placeholder' => 'Nazwa',
                )
            ))
            ->add('file', FileType::class, array(
                'label' => 'Zdjęcie w tle',
                'required' => false,
            ))
            ->add('icon', TextType::class, array(
                'label' => 'Ikona',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'Ikona'
                )
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz zmiany'
        ));
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\RoomBundle\Entity\Room',
        ));
    }

}
