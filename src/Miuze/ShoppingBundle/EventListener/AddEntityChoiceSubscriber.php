<?php


namespace Miuze\ShoppingBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;

class AddEntityChoiceSubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var string
     */
    protected $entityName;

    public function __construct(EntityManager $em, string $entityName)
    {
        $this->em = $em;
        $this->entityName = $entityName;
    }

    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SUBMIT => 'preSubmit',
        ];
    }

    public function preSubmit(FormEvent $event)
    {
        $data = $event->getData();

        if (!is_array($data) && !($data instanceof \Traversable && $data instanceof \ArrayAccess)) {
            $data = [];
        }

        // loop through all values
        $repository = $this->em->getRepository($this->entityName);
        $choices = array_map('strval', $repository->findAll());
        $className = $repository->getClassName();
        $newChoices = [];

        foreach($data as $key => $choice) {
            if(is_numeric($choice) || in_array($choice, $choices)) {
                continue;
            }
            $entity = new $className($choice);
            $newChoices[] = $entity;
            $this->em->persist($entity);
        }
        $this->em->flush();

        foreach($newChoices as $newChoice) {
            $key = array_search($newChoice->__toString(), $data);
            $data[$key] = $newChoice->getId();
        }

        $event->setData($data);
    }
}
