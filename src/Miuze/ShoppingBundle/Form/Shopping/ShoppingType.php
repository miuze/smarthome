<?php

namespace Miuze\ShoppingBundle\Form\Shopping;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Miuze\ShoppingBundle\Entity\Shopping;
use Symfony\Component\Validator\Constraints as Assert;

class ShoppingType extends AbstractType{
    public function getName(){
        return 'miuze_shopping_shopping_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder            
            ->add('email', TextType::class, array(
                'label' => 'Adres email',
                'attr' => array(
                    'placeholder' => 'Adres email',
                ),
            ))
            ->add('name', TextType::class, array(
                'label' => 'Nazwa list',
                'attr' => array(
                    'placeholder' => 'Nazwa listy',
                ),
            ))
            ->add('content', TextareaType::class, array(
                'label' => 'Notatka',
                'attr' => array(
                    'class' => 'ckeditor',
                ),
            ))
            ->add('products', ProductType::class, [
                'required' => true,
                'label'     => 'Produkty',
                'class' => 'MiuzeShoppingBundle:ShoppingProduct',
                'multiple' => true,
                'attr' => [
                    'class' => 'select-input-multiple',
                ],

            ])
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz',
                'attr' => array(
                    'class'=> 'btn btn-success'
                )
            ));


    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => Shopping::class
        ));
    }
}
