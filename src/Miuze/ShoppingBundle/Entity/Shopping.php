<?php
   
namespace Miuze\ShoppingBundle\Entity;

use Miuze\UserBundle\Entity\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Swagger\Annotations as SWG;

    
/**
 * @ORM\Entity(repositoryClass="Miuze\ShoppingBundle\Repository\ShoppingRepository")
 * @ORM\Table(name="shopping")
 */
class Shopping {
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @SWG\Property(type="int", description="Shopping id")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank
     * @SWG\Property(type="dateTime", description="Shopping creat date")
     */
    private $createDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Email()
     * @SWG\Property(type="string", description="Address email")
     */
    private $email;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     * @SWG\Property(type="string", description="Description to shopping list")
     */
    private $content;

    /**
     * @ORM\ManyToMany(
     *      targetEntity = "ShoppingProduct",
     *      inversedBy = "shopping"
     * )
     * @ORM\JoinTable(
     *      name = "shopping_to_product",
     * )
     * @SWG\Property(type="int", description="Ids for ShoppingProduct to relation with shopping")
     */
    private $products;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank
     * @SWG\Property(type="string", description="String to represent title or shop name")
     */
    private $name;

    /**
     * Constructor
     */
    public function __construct(UserInterface $user)
    {
        $this->email = $user->getEmail();
        $this->createDate = new \DateTime('now');
        $this->products = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createDate.
     *
     * @param \DateTime $createDate
     *
     * @return Shopping
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate.
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set email.
     *
     * @param string|null $email
     *
     * @return Shopping
     */
    public function setEmail($email = null)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set content.
     *
     * @param string|null $content
     *
     * @return Shopping
     */
    public function setContent($content = null)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content.
     *
     * @return string|null
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Shopping
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add product.
     *
     * @param \Miuze\ShoppingBundle\Entity\ShoppingProduct $product
     *
     * @return Shopping
     */
    public function addProduct(\Miuze\ShoppingBundle\Entity\ShoppingProduct $product)
    {
        $this->products[] = $product;

        return $this;
    }

    /**
     * Remove product.
     *
     * @param \Miuze\ShoppingBundle\Entity\ShoppingProduct $product
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeProduct(\Miuze\ShoppingBundle\Entity\ShoppingProduct $product)
    {
        return $this->products->removeElement($product);
    }

    /**
     * Get products.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducts()
    {
        return $this->products;
    }


}
