<?php
   
    namespace Miuze\ShoppingBundle\Entity;

    use Symfony\Component\Validator\Constraints as Assert;
    use Doctrine\ORM\Mapping as ORM;
    use Swagger\Annotations as SWG;

    
    /**
     * @ORM\Entity(repositoryClass="Miuze\ShoppingBundle\Repository\ShoppingRepository")
     * @ORM\Table(name="shopping_product")
     */
class ShoppingProduct {
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @SWG\Property(description="Product Id")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank
     * @SWG\Property(type="string", description="Product name")
     */
    private $name;

    /**
     * @ORM\ManyToMany(
     *      targetEntity = "Shopping",
     *      mappedBy = "products"
     * )
     */
    protected $shopping;
  

    /**
     * Constructor
     */
    public function __construct($name = '')
    {
        $this->name = $name;
        $this->shopping = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return ShoppingProduct
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add shopping.
     *
     * @param \Miuze\ShoppingBundle\Entity\Shopping $shopping
     *
     * @return ShoppingProduct
     */
    public function addShopping(\Miuze\ShoppingBundle\Entity\Shopping $shopping)
    {
        $this->shopping[] = $shopping;

        return $this;
    }

    /**
     * Remove shopping.
     *
     * @param \Miuze\ShoppingBundle\Entity\Shopping $shopping
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeShopping(\Miuze\ShoppingBundle\Entity\Shopping $shopping)
    {
        return $this->shopping->removeElement($shopping);
    }

    /**
     * Get shopping.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getShopping()
    {
        return $this->shopping;
    }
}
