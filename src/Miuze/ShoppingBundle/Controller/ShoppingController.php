<?php

namespace Miuze\ShoppingBundle\Controller;

use Miuze\ShoppingBundle\Entity\Shopping;
use Miuze\ShoppingBundle\Form\Shopping\ShoppingType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/shopping")
 */
class ShoppingController extends DefaultController
{
    public $defaultRoutePlc = 'miuze_shopping_shopping_index';

    /**
     * @Route(
     *      "/",
     *      name="miuze_shopping_shopping_index"
     * )
     */
    public function indexAction()
    {
        $list = $this->getDoctrine()->getRepository('MiuzeShoppingBundle:Shopping')->findAll();

        return $this->view(
            '@MiuzeShopping/Shopping/index.html.twig',
            [
                'action' => 'Listy zakupowe',
                'list' => $list,
            ]
        );
    }

    /**
     * @Route(
     *      "/add",
     *      name="miuze_shopping_shopping_add"
     * )
     */
    public function addAction(Request $request)
    {
        $entity = new Shopping($this->getUser());
        $form = $this->createForm(ShoppingType::class, $entity);
        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                $this->setMessage('success', 'Gratulacje, Lista dodana prawidłowo');

                return $this->redirect($this->generateUrl($this->defaultRoutePlc));
            }
            $this->setMessage('error', 'Błąd, sprawdź formularz');
        }

        return $this->view(
            '@MiuzeShopping/Shopping/add.html.twig',
            [
                'action' => 'Dodanie listy',
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route(
     *      "/edit/{id}",
     *      name="miuze_shopping_shopping_edit"
     * )
     */
    public function editAction(Request $request)
    {
        $id = $request->get('id');
        $entity = $this->getDoctrine()->getRepository('MiuzeShoppingBundle:Shopping')->findOneById($id);
        $form = $this->createForm(ShoppingType::class, $entity);
        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->merge($entity);
                $em->flush();
                $this->setMessage('success', 'Gratulacje, Lista zaktualizowana prawidłowo');

                return $this->redirect($this->generateUrl($this->defaultRoutePlc));
            }
            $this->setMessage('error', 'Błąd, sprawdź formularz');
        }

        return $this->view(
            '@MiuzeShopping/Shopping/add.html.twig',
            [
                'action' => 'Edycja listy',
                'form' => $form->createView()
            ]
        );
    }


    /**
     * @Route(
     *      "/delete/{id}",
     *      name="miuze_shopping_shopping_delete"
     * )
     */
    public function deleteAction(Request $request)
    {
        $session = $this->get('session');
        $id = $request->get('id');
        $entity = $this->getDoctrine()->getRepository('MiuzeShoppingBundle:Shopping')->findOneById($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();
        $this->setMessage('success', 'Gratulacje, Lista usunięta prawidłowo');

        return $this->redirect($this->generateUrl($this->defaultRoutePlc));
    }
}
