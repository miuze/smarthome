<?php

namespace Miuze\ShoppingBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class ShoppingRepository extends EntityRepository {

    public function getApiShoppingDetails($id) {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('n')
            ->from('MiuzeShoppingBundle:Shopping', 'n')
            ->where('n.id = :id')
            ->setParameter('id', $id);

        $result = $qb->getQuery()->getOneOrNullResult();
        return $result;
    }

    public function getApiAllShopping() {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('n')
            ->from('MiuzeShoppingBundle:Shopping', 'n')
            ->orderBy('n.id', 'DESC');

        $result = $qb->getQuery()->getResult();
        return $result;
    }

}
