<?php

namespace Miuze\AdminBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
* @Route("/")
*/
class DashboardController extends DefaultController
{
    public $defaultRoutePlc = 'miuze_admin_dashboard_index';

    /**
     * @Route(
     *      "/",
     *      name="miuze_admin_dashboard_index"
     * )
     */
    public function indexAction()
    {
        return $this->view(
            '@MiuzeAdmin/Dashboard/index.html.twig',
            [
                'action' => 'Pulpit',
            ]
        );
    }

}
