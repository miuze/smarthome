const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const bodyParser = require('body-parser')

app.use(
    bodyParser.urlencoded({
        extended: true
    })
)
app.use(bodyParser.json())

app.get('/', function (req, res) {
});

server.listen(8080, function () {
    console.log('Serwer uruchomiony');
});
var sockets = [];


function socketExist(socket) {
    var exist = false;
    var sitem;
    sockets.forEach(function (item, index) {
        if (item.userId === socket.userId) {
            exist = true;
            sitem = item;
        }
    });
    if (exist == true) {
        return sitem;
    } else {
        return false;
    }
}

function addSocket(socket) {
    sockets.push(socket);
}

app.post('/output', function(req, res){
    let outputs = req.body;
    outputs.forEach(function(output){

        sockets.forEach(function(socket){
            socket.emit('change', output);
        });
    });
});

io.on('connection', function (socket) {
    socket.on('join', function (user) {
        socket.userId = user.id;
        socket.userName = user.name;
        console.log('Użytkownik ' + socket.userName + ' właśnie sie zalogował');

        var socketEx = socketExist(socket);
        if (socketEx == false) {
            addSocket(socket);
        } else {
            sendHelloMsgToEnother(socket);
        }
    });
    socket.on('disconnect', function () {
        sockets.forEach(function (user, index) {
            if (user !== socket) {
                user.emit('news', 'Użytnownik ' + socket.userName + ' opuścił aplikację.');
            }
        });
        console.log('Usunięty');
//        removeUserFromUsers(socket);
    });

});

function sendHelloMsgToEnother(socket) {
    sockets.forEach(function (item, index) {
        if (item.userId !== socket.userId) {
            console.log(socket.userName);
            item.emit('news', 'Użytkownik o nazwie: ' + socket.userName + ' właśnie się zalogował.');
        }
    });
}