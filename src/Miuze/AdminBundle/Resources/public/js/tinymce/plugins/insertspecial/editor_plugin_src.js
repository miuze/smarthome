(function() {
    tinymce.create('tinymce.plugins.CodePlugin', {
    // funkcja uruchamiania podczas wczytywania wtyczki
	init : function(ed, url) {
	    //tworzymy funkcje odpowiedzialna za utworzenie okna popup
	    ed.addCommand('mceCode', function() {
		ed.windowManager.open({
		    file : url + '/dialog.html', //sciezka z zawartoscia okna
		    width : 560,
		    height : 400,
		    inline: 1
		},
		{
		    plugin_url : url // Plugin absolute URL
		});
	    });

	    //tworzymy przycisk uruchamiający popup
	    ed.addButton('insertspecial', {
		title : 'Wybierz element do osadzenia',
		cmd : 'mceCode', //metoda wykonywana po nacisnieciu przycisku
		image : url + '/img/insertcode.png' //sciezka do obrazka
	    });
	}
    }); //end tinymce.create()

    // Rejestrujemy plugin
    tinymce.PluginManager.add('insertspecial', tinymce.plugins.CodePlugin);
})();