var _animationSpeed = 10;
$(document).ready(function () {
    // if ($(window).width() < 960) {
    //     $('.container-row').addClass('scrollbar');
    // }
    if($(window).width() < 960){
        $('aside').addClass('mobile');
        $('.btn-mobile-navigation').addClass('mobile');
    }
    if ($(window).width() > 960) {
        $(".scrollbar").mCustomScrollbar({
            axis: "y",
            theme: "dark-3"
        });
    }
    showRoom();
    toggleMenu();
    changeValue();
    ajaxGet();
    AccordionNotes();
    removeNote();
    selectRender();
    setHeightAside();
});

function setHeightAside() {
        var navHeight = $('.main-nav').height() + 20;
        var windowHeight = $(window).height();
        var bodyHeight = windowHeight - navHeight;
        $('.main-container').css('height', bodyHeight + 'px');
        $('.scrollbar').css('height', bodyHeight + 'px');
}

function showRoom() {
    $('.btn-mobile-navigation').on('click', function(){
        $('aside').toggleClass('active');
    });
}

function selectRender(){
    $(".select-input").each(function(index, elem){
        $(elem).select2({
        });
    });
    $(".select-input-multiple").each(function(index, elem){
        $(elem).select2({
            tags: true,
        });
    });
}

function AccordionNotes() {
    if ($('.collapse').length) {
        $('.collapse').collapse({
//            toggle: true
        });
    }
}

function toggleMenu() {
    $('.btn-navigation').on('click', function () {
        $('nav').toggleClass('active');
    });
    $('.bg-nav').on('click', function () {
        $('nav').removeClass('active');
    });
}

function ajaxGet() {
    $('.ajax-get').on('click', function (e) {
        e.preventDefault();
        var link = $(this).attr('href');
        $.ajax({
            type: 'GET',
            url: link,
            success: function (data) {
                $('.ajax-message').html('<div class="alert alert-success">' + data + '</div>');
                showAjaxMsg();
            },
            error: function (data) {
                alert('Proszę odświerzyć stronę i spróbować ponownie');
            }
        });
    });
}

function changeValue() {
    $('.form').on('click', 'label', function (event, autoClick) {
        var form = $($(this)[0].form);
        if (autoClick == true) {
        } else {
            sendAjax(form);
        }
    });
}
function sendAjax(form) {
    console.log(form);
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize(),
        success: function (data) {
            $('.ajax-message').html('<div class="alert alert-success">' + data + '</div>');
            showAjaxMsg();
        },
        error: function (data) {
            alert(data);
            backClick(form);
        }
    });
}

function backClick(form) {
    form.find('label').trigger('click', true);
}

function showAjaxMsg() {
    if ($('.ajax-message').length) {
        setTimeout(function () {
            $('.ajax-message').addClass('active');
            setTimeout(function () {
                $('.ajax-message').removeClass('active');
            }, 7000);
        }, 500);

    }
}

function removeNote() {
    $('.ajax-note-delete').on('click', function (e) {
        e.preventDefault();
        var link = $(this);
        $.ajax({
            type: 'GET',
            url: link.attr('href'),
//            data: link.serialize(),
            success: function (data) {
                $('.ajax-result').html(data);
                data = JSON.parse(data);
                if (data.status == 'success') {
                    $('.ajax-message').html('<div class="alert alert-' + data.status + '">' + data.msg + '</div>');
                    showAjaxMsg();
                    $('.card-' + data.id).remove();
                } else if (data.status == 'danger') {
                    $('.ajax-message').html('<div class="alert alert-' + data.status + '">' + data.msg + '</div>');
                    showAjaxMsg();
                } else {
                    console.log('Nieznany błąd systemu');
                }
            },
            error: function (data) {
                alert('Proszę odświerzyć stronę i spróbować ponownie');
            }
        });

    });
}
