function parse(){
    text = top.tinymce.activeEditor.selection.getContent({
        format : 'text'
    });
    
    var input = $('#content').val().split('-');
    var p = input[0];
    var t = input[1];
    var hrefTitle = $('#hrefTitle').val();

    if(!text){
        var anchor = top.tinymce.activeEditor.selection.getNode();
        bHref = '/{@urlBuilder:' + p + '-' + t + '@}';
        text = anchor.innerHtml;
        tinymcePopup.editor.execCommand('mceInsertLink', false, {
            href : bHref, 
            innerHtml : text,
            title : hrefTitle
        });
    } else{
        bHref = '/{@urlBuilder:' + p + '-' + t + '@}';
        bInnerHtml = text;
        top.tinymce.activeEditor.execCommand('mceInsertLink', false, {
            href : bHref, 
            innerHtml : text,
            title : hrefTitle
        });
    }

    top.tinymce.activeEditor.windowManager.close();
    return false;
}

function updateActive(){
    var anchor = top.tinymce.activeEditor.selection.getNode();
    var href = anchor.href;
    var title = anchor.title;
    if(href != undefined){
        var regxp = /urlBuilder:([0-9]+)-([0-9]+)/;
        cont = href.match(regxp)[1];
        id = href.match(regxp)[2];
        $('#content').find('option[value='+cont+'-'+id+']').attr('selected', 'selected');
        $('#hrefTitle').val(title);
    }
}

function createPageList(){
    pages = top.tinymce.settings.widgetData.pageList;
    goThrough(pages, 0);
}

function count(data){
    var count = 0;
    for(var i in data){
        count++;
    }
    return count;
}

function setIndent(lvl){
    var indent = '';
    for(var i = 0; i < lvl ; i++){
        indent += '-';
    }
    return indent;
}

function goThrough(data, lvl){
    for(var i in data){
        html = data[i].title;
        var indent = setIndent(lvl);
        html = '<option value="' + data[i].menuId + '-' + data[i].id + '">' + indent + ' ' + html + '</option>';
        $('#content').append(html);
	
        if(count(data[i].children) > 0){
            setTimeout(goThrough(data[i].children, lvl+1), 1);
        }
    }
}

$(document).ready(function(){
    createPageList();
    updateActive();
});
