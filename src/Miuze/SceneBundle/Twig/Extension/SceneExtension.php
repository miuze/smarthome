<?php

declare(strict_types=1);

namespace Miuze\SceneBundle\Twig\Extension;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Miuze\SceneBundle\Entity\Scene;

class SceneExtension extends \Twig_Extension
{
    /**
     * @var \Doctrine\Bundle\DoctrineBundle\Registry
     */
    private $doctrine;

    function __construct(Registry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function getName()
    {
        return 'miuze_scene_scene_extension';
    }

    public function getFunctions(): array
    {
        return array(
            new \Twig_SimpleFunction(
                'render_scene', [$this, 'renderscene'],
                [
                    'is_safe' => ['html'],
                    'needs_environment' => true,
                ]
            ),

        );
    }

    public function renderScene(\Twig_Environment $environment, Scene $scene)
    {
        $repo = $this->doctrine->getRepository('MiuzeSceneBundle:Scene');
        $scene = $repo->findOneBy(['id' => $scene->getId()]);
        if (\is_null($scene)) {
            return;
        }
        if (empty($scene->getSceneMarkers())) {
            throw new \Exception('Nie ustawiono wyjść dla sceny ' . $scene->gerName());
        }
        return $environment->render(
            '@MiuzeScene/Twig/Scene/scene.html.twig',
            array(
                'scene' => $scene
            )
        );
    }

}
