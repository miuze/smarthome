<?php

declare(strict_types=1);

namespace Miuze\SceneBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Miuze\SceneBundle\Entity\Scene;

class SceneRepository extends EntityRepository
{
    public function getSceneById($id): ?Scene
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('s, sm, bm, m, v')
            ->from('MiuzeSceneBundle:Scene', 's')
            ->leftJoin('s.sceneMarkers', 'sm')
            ->innerJoin('sm.marker', 'bm')
            ->innerJoin('bm.marker', 'm')
            ->innerJoin('sm.value' , 'v')
            ->where('s.id = :id')
            ->setParameter('id', $id);
        return $qb->getQuery()->getOneOrNullResult();
    }

}