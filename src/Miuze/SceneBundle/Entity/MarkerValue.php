<?php

declare(strict_types=1);

namespace Miuze\SceneBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

    /**
    * @ORM\Entity
    * @ORM\Table(name="scene_marker_value")
    */    
class MarkerValue {

    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(
     *      targetEntity = "SceneMarker",
     *      mappedBy = "value",
     *      cascade={"all"}
     * )
     */
    protected $marker;

    /**
     * @ORM\Column(type="boolean")
     * @Assert\NotBlank()
     */
    private $value = 1;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     */
    private $name;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->marker = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param boolean $value
     *
     * @return MarkerValue
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return boolean
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Add marker
     *
     * @param \Miuze\SceneBundle\Entity\SceneMarker $marker
     *
     * @return MarkerValue
     */
    public function addMarker(\Miuze\SceneBundle\Entity\SceneMarker $marker)
    {
        $this->marker[] = $marker;

        return $this;
    }

    /**
     * Remove marker
     *
     * @param \Miuze\SceneBundle\Entity\SceneMarker $marker
     */
    public function removeMarker(\Miuze\SceneBundle\Entity\SceneMarker $marker)
    {
        $this->marker->removeElement($marker);
    }

    /**
     * Get marker
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMarker()
    {
        return $this->marker;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return MarkerValue
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
