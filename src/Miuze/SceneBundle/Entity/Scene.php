<?php

declare(strict_types=1);

namespace Miuze\SceneBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity(repositoryClass="Miuze\SceneBundle\Repository\SceneRepository")
* @ORM\Table(name="scenes")
*/
class Scene {
    

    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=255
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $icon;
        
    
    /**
     * @ORM\OneToMany(
     *      targetEntity = "SceneMarker",
     *      mappedBy = "scene",
     *      cascade={"all"},
     *      orphanRemoval=true
     * )
     */
    protected $sceneMarkers;

    /**
     * @ORM\OneToMany(
     *      targetEntity = "Miuze\RoomBundle\Entity\ViewItem",
     *      mappedBy = "scene",
     * )
     */
    private $view;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->sceneMarkers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Scene
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set icon
     *
     * @param string $icon
     *
     * @return Scene
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Add sceneMarker
     *
     * @param \Miuze\SceneBundle\Entity\SceneMarker $sceneMarker
     *
     * @return Scene
     */
    public function addSceneMarker(\Miuze\SceneBundle\Entity\SceneMarker $sceneMarker)
    {
        $this->sceneMarkers[] = $sceneMarker;
        $sceneMarker->setScene($this);

        return $this;
    }

    /**
     * Remove sceneMarker
     *
     * @param \Miuze\SceneBundle\Entity\ActionMarker $sceneMarker
     */
    public function removeSceneMarker(\Miuze\SceneBundle\Entity\SceneMarker $sceneMarker)
    {
        $this->sceneMarkers->removeElement($sceneMarker);
    }

    /**
     * Get sceneMarkers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSceneMarkers()
    {
        return $this->sceneMarkers;
    }

    /**
     * Add view.
     *
     * @param \Miuze\RoomBundle\Entity\ViewItem $view
     *
     * @return Scene
     */
    public function addView(\Miuze\RoomBundle\Entity\ViewItem $view)
    {
        $this->view[] = $view;

        return $this;
    }

    /**
     * Remove view.
     *
     * @param \Miuze\RoomBundle\Entity\ViewItem $view
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeView(\Miuze\RoomBundle\Entity\ViewItem $view)
    {
        return $this->view->removeElement($view);
    }

    /**
     * Get view.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getView()
    {
        return $this->view;
    }
}
