<?php

declare(strict_types=1);

namespace Miuze\SceneBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

    /**
    * @ORM\Entity
    * @ORM\Table(name="scene_markers")
    */    
class SceneMarker {

    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Scene",
     *      inversedBy = "sceneMarkers"
     * )
     * @ORM\JoinColumn(
     *      name = "scene_id",
     *      referencedColumnName= "id",
     *      onDelete = "CASCADE"
     * )
     */
    private $scene;
    
    
    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Miuze\PlcBundle\Entity\ButtonMarker",
     *      inversedBy = "scene"
     * )
     * @ORM\JoinColumn(
     *      name = "button_marker_id",
     *      referencedColumnName= "id",
     *      onDelete = "CASCADE"
     * )
     */
    private $marker;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "MarkerValue",
     *      inversedBy = "marker"
     * )
     * @ORM\JoinColumn(
     *      name = "marker_value_id",
     *      referencedColumnName= "id",
     *      onDelete = "CASCADE"
     * )
     */
    private $value;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set action
     *
     * @param string $action
     *
     * @return ActionMarker
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set scene
     *
     * @param \Miuze\SceneBundle\Entity\Scene $scene
     *
     * @return ActionMarker
     */
    public function setScene(\Miuze\SceneBundle\Entity\Scene $scene = null)
    {
        $this->scene = $scene;

        return $this;
    }

    /**
     * Get scene
     *
     * @return \Miuze\SceneBundle\Entity\Scene
     */
    public function getScene()
    {
        return $this->scene;
    }

    /**
     * Set markers
     *
     * @param \Miuze\PlcBundle\Entity\ButtonMarker $markers
     *
     * @return ActionMarker
     */
    public function setMarker(\Miuze\PlcBundle\Entity\ButtonMarker $marker = null)
    {
        $this->marker = $marker;

        return $this;
    }

    /**
     * Get markers
     *
     * @return \Miuze\PlcBundle\Entity\ButtonMarker
     */
    public function getMarker()
    {
        return $this->marker;
    }

    /**
     * Set value
     *
     * @param \Miuze\SceneBundle\Entity\MarkerValue $value
     *
     * @return ActionMarker
     */
    public function setValue(\Miuze\SceneBundle\Entity\MarkerValue $value = null)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return \Miuze\SceneBundle\Entity\MarkerValue
     */
    public function getValue()
    {
        return $this->value;
    }
}
