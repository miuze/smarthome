<?php

namespace Miuze\SceneBundle\Form\Scene;

use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Miuze\SceneBundle\Entity\ActionMarker;
use Miuze\SceneBundle\Form\Scene\MarkerSceneType;
use Miuze\SceneBundle\Entity\Scene;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class SceneType extends AbstractType {

    public function getName() {
        return 'miuze_scene_scene';
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Nazwa',
                'required' => true,
                'attr' => array(
                    'placeholder' => 'Nazwa'
                )
            ))
            ->add('icon', TextType::class, array(
                'label' => 'Ikona Css',
                'required' => true,
                'attr' => array(
                    'placeholder' => 'Css'
                )
            ))
            ->add('sceneMarkers', CollectionType::class, array(
                'label' => false,
                'allow_add' => true,
                'entry_type' => MarkerSceneType::class,
                'by_reference' => false,
                'allow_delete' =>true,
                'entry_options' => [
                    'attr' => ['class' => 'buttonMarker-box'],
                ],
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz zmiany'
        ));
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => Scene::class,
            'csrf_protection' => false,
        ));
    }

}
