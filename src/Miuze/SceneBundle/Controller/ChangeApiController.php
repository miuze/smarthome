<?php

namespace Miuze\SceneBundle\Controller;

use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\FOSRestController;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/scene")
 */
class ChangeApiController extends FOSRestController
{
    /**
     * @Put("/change", name="_api_scene_change")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Zmiana zastosowana"
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Błąd parametrów",
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Blędna autoryzacja",
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Blędna komunikacja ze sterownikiem",
     * )
     * @SWG\Parameter(
     *     name="payload",
     *     in="body",
     *     required=true,
     *     description="Podaj id sceny a system uruchomi przypisane akcje.",
     *     @SWG\Schema(
     *          @SWG\Property(
     *              property="id",
     *              type="integer",
     *              example=1
     *          )
     *      ),
     * )
     * @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     required=true,
     *     type="string",
     *     description="Token z logowania",
     *     )
     * @SWG\Tag(name="Scene")
     */
    public function putAction(Request $request)
    {
        $id = $request->get('id');
        if (is_null($id)) {
            return new JsonResponse('Brak parametru', Response::HTTP_BAD_REQUEST);
        }
        $entity = $this->getDoctrine()
            ->getRepository('MiuzeSceneBundle:Scene')
            ->getSceneById($id);
        $plcService = $this->get('Miuze\PlcBundle\Service\Modbus');
        foreach ($entity->getSceneMarkers() as $marker) {
            $result = $plcService->sendStatusMarkerToPlc(
                $marker->getMarker()->getMarker(),
                $marker->getValue()->getValue()
            );
            if($result === false){
                return new JsonResponse('Błąd w komunikacji ze sterownikiem', Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }

        return new JsonResponse(sprintf('%s uruchomiona.', $entity->getName()));
    }
}
