<?php

namespace Miuze\SceneBundle\Controller;

use Miuze\SceneBundle\Entity\Scene;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/scene")
 */
class ChangeController extends Controller
{
    /**
     * @Route(
     *      "/change/{id}",
     *      name="miuze_scene_change_change"
     * )
     */
    public function changeAction(Request $request)
    {
        /**
         * @var Scene
         */
        $entity = $this->getDoctrine()
            ->getRepository('MiuzeSceneBundle:Scene')
            ->getSceneById($request->attributes->getInt('id'));
        $plcService = $this->get('Miuze\PlcBundle\Service\Modbus');
        foreach ($entity->getSceneMarkers() as $marker) {
            $result = $plcService->sendStatusMarkerToPlc(
                $marker->getMarker()->getMarker(),
                $marker->getValue()->getValue()
            );
            if ($result === false) {
                return new JsonResponse(
                    [
                        'status' => 'danger',
                        'msg' => 'Błąd w komunikacji ze sterownikiem',
                    ]
                );
            }
        }
        return new JsonResponse(
            [
                'status' => 'success',
                'msg' => sprintf('Scena %s uruchomiona', $entity->getName()),
            ]
        );
    }
}
