<?php

namespace Miuze\SceneBundle\Controller;

use Miuze\SceneBundle\Controller\DefaultController;
use Miuze\SceneBundle\Entity\Scene;
use Miuze\SceneBundle\Entity\ActionMarker;
use Miuze\SceneBundle\Form\Scene\SceneType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
* @Route("/scene")
*/
class SceneController extends DefaultController
{
    public $defaultRoutePlc = 'miuze_scene_scene_index';

    /**
     * @Route(
     *      "/",
     *      name="miuze_scene_scene_index"
     * )
     */
    public function indexAction()
    {
        $list = $this->getDoctrine()->getRepository('MiuzeSceneBundle:Scene')->findAll();

        return $this->view(
            '@MiuzeScene/Scene/index.html.twig',
            [
                'action' => 'Lista scen',
                'list' => $list,
            ]
        );
    }
    
    /**
     * @Route(
     *      "/add",
     *      name="miuze_scene_scene_add"
     * )
     */
    public function addAction(Request $request)
    {
        $entity = new Scene();
        $form = $this->createForm(SceneType::class, $entity);
        if($request->isMethod(Request::METHOD_POST)){
            $form->handleRequest($request);
            if($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                $this->setMessage('success', 'Zapisano ustawienia');

                return $this->redirect($this->generateUrl($this->defaultRoutePlc));
            }
        }

        return $this->view(
            '@MiuzeScene/Scene/add.html.twig',
            [
                'action' => 'Dodaj scenę',
                'form' => $form->createView(),
            ]
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name="miuze_scene_scene_edit"
     * )
     */
    public function editAction(Request $request)
    {
        $entity = $this->getDoctrine()
            ->getRepository('MiuzeSceneBundle:Scene')
            ->findOneById($request->attributes->getInt('id'));
        if($entity == null){
            $this->setMessage('danger', 'Nie znaleziono sceny');

            return $this->redirect($this->generateUrl($this->defaultRoutePlc));
        }
        $form = $this->createForm(SceneType::class, $entity);
        if($request->isMethod(Request::METHOD_POST)){
            $form->handleRequest($request);
            if($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $em->merge($entity);
                $em->flush();
                $this->setMessage('success', 'Zapisano ustawienia');

                return $this->redirect($this->generateUrl($this->defaultRoutePlc));
            }
        }

        return $this->view(
            '@MiuzeScene/Scene/add.html.twig',
            [
                'action' => 'Edycja sceny',
                'form' => $form->createView(),
            ]
        );
    }
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="miuze_scene_scene_delete"
     * )
     */
    public function deleteAction(Request $request)
    {
        $entity = $this->getDoctrine()
            ->getRepository('MiuzeSceneBundle:Scene')
            ->findOneById($request->attributes->getInt('id'));
        if($entity == null){
            $this->setMessage('danger', 'Nie znaleziono sceny');

            return $this->redirect($this->generateUrl($this->defaultRoutePlc));
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();
        $this->setMessage('success', 'Scena usunięta');

        return $this->redirect($this->generateUrl($this->defaultRoutePlc));
    }

}
