<?php

declare(strict_types=1);

namespace Miuze\PlcBundle\Util\Interfaces;

use Miuze\PlcBundle\Entity\Controller;

interface MarkerInterface
{
    public function setName(string $name);

    public function getName(): ?string;

    public function setValue(bool $value);

    public function getValue(): ?bool;

    public function setAddress(int $address);

    public function getAddress(): ?int;

    public function setAddressStatus(int $status);

    public function getAddressStatus(): ?int;

    public function setController(Controller $controller);

    public function getController();
}