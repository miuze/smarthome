<?php


namespace Miuze\PlcBundle\Util\Enum;


class MessageStatus
{
    public const SUCCESS = 'success';
    public const DANGER = 'danger';

}