<?php


namespace Miuze\PlcBundle\Util\Enum;


class MarkerStatus
{
    public const ON = 'Włączono';
    public const OFF = 'Wyłączono';
}