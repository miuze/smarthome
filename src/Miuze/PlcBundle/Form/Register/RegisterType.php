<?php

declare(strict_types=1);

namespace Miuze\PlcBundle\Form\Register;

use Miuze\PlcBundle\Entity\Register;
use Miuze\PlcBundle\Repository\MarkerRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class RegisterType extends AbstractType{
    public function getName(){
        return 'miuze_plc_register_marker';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder            
            ->add('name', TextType::class, array(
                'label' => 'Nazwa punktu',
                'attr' => array(
                    'placeholder' => 'Nazwa rejestru',
                ),
            ))
            ->add('registerNumber', NumberType::class, array(
                'label' => 'Numer rejestru',
                'attr' => array(
                    'placeholder' => 'Numer rejestru',
                ),
            ))
            ->add('marker', EntityType::class, array(
                'class' => 'MiuzePlcBundle:Marker',
                'query_builder' => function (MarkerRepository $er) {
                    return $er->createQueryBuilder('m');
                },
                'label' => 'Marker',
                'choice_label' => function ($marker) {
                    return sprintf('%s - %s', $marker->getName(), $marker->getAddress());
                },
                'attr' => [
                    'class' => 'select-input'
                ]
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz',
                'attr' => array(
                    'class'=> 'btn btn-success'
                )
            ));            
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => Register::class
        ));
    }
}
