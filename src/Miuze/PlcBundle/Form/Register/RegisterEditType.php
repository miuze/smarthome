<?php

declare(strict_types=1);

namespace Miuze\PlcBundle\Form\Register;

use Miuze\PlcBundle\Entity\Register;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class RegisterEditType extends AbstractType{
    public function getName(){
        return 'miuze_plc_register_edit';
    }
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder            
            ->add('name', TextType::class, array(
                'label' => 'Nazwa punktu',
                'attr' => array(
                    'placeholder' => 'Nazwa rejestru',
                ),
            ))
            ->add('registerNumber', NumberType::class, array(
                'label' => 'Numer rejestru',
                'attr' => array(
                    'placeholder' => 'Numer rejestru',
                ),
            ))
            ->add('value', NumberType::class, array(
                'label' => 'Czas w sekundach',
                'attr' => array(
                    'placeholder' => 'Czas w sekundach',
                ),
            ))
            ->add('marker', EntityType::class, array(
                'class' => 'MiuzePlcBundle:Marker',
                'label' => 'Marker',
                'choice_label' => 'name',
                'attr' => [
                    'class' => 'select-input'
                ]
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz',
                'attr' => array(
                    'class'=> 'btn btn-success'
                )
            ));            
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => Register::class
        ));
    }
}
