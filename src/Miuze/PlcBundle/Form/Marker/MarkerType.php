<?php

declare(strict_types=1);

namespace Miuze\PlcBundle\Form\Marker;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MarkerType extends AbstractType
{

    public function getName()
    {
        return 'miuze_plc_marker';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                TextType::class,
                array(
                    'label' => 'Nazwa',
                    'required' => true,
                    'attr' => array(
                        'placeholder' => 'Krótki opis'
                    )
                )
            )
            ->add(
                'address',
                NumberType::class,
                array(
                    'label' => 'Adres markera',
                    'required' => true
                )
            )
            ->add(
                'addressStatus',
                NumberType::class,
                array(
                    'label' => 'Adres markera statusu',
                    'required' => true
                )
            )
            ->add(
                'value',
                ChoiceType::class,
                array(
                    'label' => 'Stan markera',
                    'required' => true,
                    'choices' => array(
                        'Stan wysoki' => true,
                        'Stan niski' => false,
                    )
                )
            )
            ->add(
                'controller',
                EntityType::class,
                array(
                    'label' => 'Sterownik',
                    'class' => 'MiuzePlcBundle:Controller',
                    'choice_label' => 'name',
                )
            )
            ->add(
                'submit',
                SubmitType::class,
                array(
                    'label' => 'Zapisz zmiany'
                )
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Miuze\PlcBundle\Entity\Marker',
            )
        );
    }

}
