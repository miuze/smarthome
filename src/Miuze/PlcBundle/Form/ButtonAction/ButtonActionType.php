<?php

namespace Miuze\PlcBundle\Form\ButtonAction;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ButtonActionType extends AbstractType {

    public function getName() {
        return 'miuze_plc_button-action';
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Nazwa',
                'required' => true,
                'attr' => array(
                    'placeholder' => 'Nazwa'
                )
            ))
            ->add('class', TextType::class, array(
                    'label' => 'Klasa css',
                    'required' => false,
                    'attr' => array(
                        'placeholder' => 'fa fa-angle-up'
                    )
                ))
            ->add('action', TextType::class, array(
                'label' => 'Akcja',
                'required' => true,
                'attr' => array(
                    'placeholder' => 'Akcja'
                )
            ))
            ->add('alwaysTrue', ChoiceType::class, array(
                'label' => 'Zawsze stan wysoki',
                'choices'=> [
                    'Tak' => 1,
                    'Nie' => 0
                ]

            ))
                ->add('submit', SubmitType::class, array(
                    'label' => 'Zapisz zmiany'
        ));
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\PlcBundle\Entity\ButtonAction',
        ));
    }

}
