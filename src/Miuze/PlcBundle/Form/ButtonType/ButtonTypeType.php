<?php

namespace Miuze\PlcBundle\Form\ButtonType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ButtonTypeType extends AbstractType {

    public function getName() {
        return 'miuze_plc_button-type';
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('name', TextType::class, array(
                    'label' => 'Nazwa',
                    'required' => true,
                    'attr' => array(
                        'placeholder' => 'Nazwa'
                    )
                ))
            ->add('action', TextType::class, array(
                'label' => 'Akcja',
                'required' => true,
                'attr' => array(
                    'placeholder' => 'Akcja'
                )
            ))
                ->add('submit', SubmitType::class, array(
                    'label' => 'Zapisz zmiany'
        ));
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\PlcBundle\Entity\ButtonType',
            'csrf_protection' => false,
        ));
    }

}
