<?php

namespace Miuze\PlcBundle\Form\Plc;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class PlcType extends AbstractType {

    public function getName() {
        return 'miuze_plc_controller';
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('name', TextType::class, array(
                    'label' => 'Nazwa',
                    'required' => true,
                    'attr' => array(
                        'placeholder' => 'Nazwa'
                    )
                ))
                ->add('unit', NumberType::class, array(
                    'label' => 'Id jednostki',
                    'required' => FALSE
                ))
                ->add('ip', TextType::class, array(
                    'label' => 'Adres IPv4',
                    'attr' => array(
                        'placeholder' => 'IP'
                    )
                ))
                ->add('submit', SubmitType::class, array(
                    'label' => 'Zapisz zmiany'
        ));
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\PlcBundle\Entity\Controller',
        ));
    }

}
