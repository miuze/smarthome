<?php

namespace Miuze\PlcBundle\Form\Button;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class MarkerType extends AbstractType {

    public function getName() {
        return 'miuze_plc_button-marker';
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Nazwa',
                'required' => true,
                'attr' => array(
                    'placeholder' => 'Nazwa'
                )
            ))
            ->add('marker', EntityType::class, array(
                'label' => 'Marker',
                'attr' => array(
                    'class' => 'select-input',
                ),
                'class' => 'MiuzePlcBundle:Marker',
                'choice_label' => function($marker){
                    return $marker->getName() . ' - '. $marker->getAddress();
                },
            ))
            ->add('markerAction', EntityType::class, array(
                'label' => 'Akcja po kliknięciu przycisku',
                'class' => 'MiuzePlcBundle:ButtonAction',
                'choice_label' => 'name',
            ));
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\PlcBundle\Entity\ButtonMarker',
            'csrf_protection' => false,
        ));
    }

}
