<?php

namespace Miuze\PlcBundle\Form\Button;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ButtonType extends AbstractType {

    public function getName() {
        return 'miuze_plc_button';
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Nazwa',
                'required' => true,
                'attr' => array(
                    'placeholder' => 'Nazwa'
                )
            ))
            ->add('buttonType', EntityType::class, array(
                'label' => 'Typ przycisku',
                'class' => 'MiuzePlcBundle:ButtonType',
                'choice_label' => 'name',
            ))
            ->add('buttonMarker', CollectionType::class, array(
                'label' => false,
                'allow_add' => true,
                'entry_type' => MarkerType::class,
                'by_reference' => false,
//                'entry_options' => [
//                    'attr' => ['class' => 'buttonMarker-box'],
//                ],
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz zmiany'
        ));
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Miuze\PlcBundle\Entity\Button',
            'csrf_protection' => false,
        ));
    }

}
