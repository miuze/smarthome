<?php

declare(strict_types=1);

namespace Miuze\PlcBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Miuze\PlcBundle\Util\Interfaces\MarkerInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity()
 * @ORM\Table(name="button_action")
 */
class ButtonAction
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="text", length=50)
     * @Assert\NotBlank()
     * @Assert\Length(max = 50)
     */
    private $name;

    /**
     * @ORM\Column(type="text", length=50)
     * @Assert\NotBlank()
     * @Assert\Length(max = 50)
     */
    private $action;

    /**
     * @ORM\Column(type="boolean")
     */
    private $alwaysTrue = 0;

    /**
     * @ORM\Column(type="text", length=50)
     * @Assert\NotBlank()
     * @Assert\Length(max = 50)
     */
    private $class;

    /**
     * @ORM\OneToMany(
     *      targetEntity = "ButtonMarker",
     *      mappedBy = "markerAction"
     * )
     */
    private $buttonMarker;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->buttonMarker = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setAction(string $action)
    {
        $this->action = $action;

        return $this;
    }

    public function getAction(): ?string
    {
        return $this->action;
    }

    public function getClass(): ?string
    {
        return $this->class;
    }

    public function setClass(?string $class): void
    {
        $this->class = $class;
    }

    /**
     * Add buttonMarker
     *
     * @param \Miuze\PlcBundle\Entity\ButtonMarker $buttonMarker
     *
     * @return ButtonAction
     */
    public function addButtonMarker(\Miuze\PlcBundle\Entity\ButtonMarker $buttonMarker)
    {
        $this->buttonMarker[] = $buttonMarker;

        return $this;
    }

    /**
     * Remove buttonMarker
     *
     * @param \Miuze\PlcBundle\Entity\ButtonMarker $buttonMarker
     */
    public function removeButtonMarker(\Miuze\PlcBundle\Entity\ButtonMarker $buttonMarker)
    {
        $this->buttonMarker->removeElement($buttonMarker);
    }

    /**
     * Get buttonMarker
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getButtonMarker()
    {
        return $this->buttonMarker;
    }

    /**
     * Set alwaysTrue
     *
     * @param boolean $alwaysTrue
     *
     * @return ButtonAction
     */
    public function setAlwaysTrue($alwaysTrue)
    {
        $this->alwaysTrue = $alwaysTrue;

        return $this;
    }

    /**
     * Get alwaysTrue
     *
     * @return boolean
     */
    public function getAlwaysTrue()
    {
        return $this->alwaysTrue;
    }
}
