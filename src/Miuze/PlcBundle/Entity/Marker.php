<?php

declare(strict_types=1);

namespace Miuze\PlcBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Miuze\PlcBundle\Util\Interfaces\MarkerInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="Miuze\PlcBundle\Repository\MarkerRepository")
 * @ORM\Table(name="markers")
 * @UniqueEntity(fields={"address"})
 */
class Marker implements MarkerInterface
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="text", length=50)
     * @Assert\NotBlank()
     * @Assert\Length(max = 50)
     */
    private $name;

    /**
     * @ORM\Column(type="boolean")
     */
    private $value;

    /**
     * @ORM\Column(type="integer", length = 4, unique = true)
     * @Assert\NotBlank
     * @Assert\Type("integer")
     * @Assert\Length(
     *      min=1,
     *      max=4,
     *     minMessage="Pole musi zawierać przynajmniej {{ limit }} cyfrę",
     *     maxMessage="Pole musi zawierać maksymalnie {{ limit }} cyfry"
     * )
     */
    private $address;

    /**
     * @ORM\Column(type="integer", length = 4)
     * @Assert\NotBlank
     * @Assert\Type("integer")
     * @Assert\Length(
     *      min=1,
     *      max=4,
     *     minMessage="Pole musi zawierać przynajmniej {{ limit }} cyfrę",
     *     maxMessage="Pole musi zawierać maksymalnie {{ limit }} cyfry"
     * )
     */
    private $addressStatus;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Controller",
     *      inversedBy = "markers"
     * )
     * @ORM\JoinColumn(
     *      name = "unit_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    protected $controller;

    /**
     * @ORM\OneToMany(
     *      targetEntity = "ButtonMarker",
     *      mappedBy = "marker"
     * )
     */
    private $buttons;

    /**
     * @ORM\OneToMany(
     *      targetEntity = "Miuze\PlcBundle\Entity\Register",
     *      mappedBy = "marker"
     * )
     */
    protected $register;


    public function __construct()
    {
        $this->value = true;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setValue(bool $value)
    {
        $this->value = $value;
    }

    public function getValue(): ?bool
    {
        return $this->value;
    }

    public function setAddress(int $address)
    {
        $this->address = $address;
    }

    public function getAddress(): ?int
    {
        return $this->address;
    }

    public function setController(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function getController()
    {
        return $this->controller;
    }



    /**
     * Set addressStatus
     *
     * @param integer $addressStatus
     *
     * @return Marker
     */
    public function setAddressStatus(int $addressStatus)
    {
        $this->addressStatus = $addressStatus;

        return $this;
    }

    /**
     * Get addressStatus
     *
     * @return integer
     */
    public function getAddressStatus(): ?int
    {
        return $this->addressStatus;
    }

    /**
     * Add button
     *
     * @param \Miuze\PlcBundle\Entity\ButtonMarker $button
     *
     * @return Marker
     */
    public function addButton(\Miuze\PlcBundle\Entity\ButtonMarker $button)
    {
        $this->buttons[] = $button;

        return $this;
    }

    /**
     * Remove button
     *
     * @param \Miuze\PlcBundle\Entity\ButtonMarker $button
     */
    public function removeButton(\Miuze\PlcBundle\Entity\ButtonMarker $button)
    {
        $this->buttons->removeElement($button);
    }

    /**
     * Get buttons
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getButtons()
    {
        return $this->buttons;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Marker
     */
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Add register.
     *
     * @param \Miuze\PlcBundle\Entity\Register $register
     *
     * @return Marker
     */
    public function addRegister(\Miuze\PlcBundle\Entity\Register $register)
    {
        $this->register[] = $register;

        return $this;
    }

    /**
     * Remove register.
     *
     * @param \Miuze\PlcBundle\Entity\Register $register
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeRegister(\Miuze\PlcBundle\Entity\Register $register)
    {
        return $this->register->removeElement($register);
    }

    /**
     * Get register.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRegister()
    {
        return $this->register;
    }
}
