<?php

declare(strict_types=1);

namespace Miuze\PlcBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Miuze\PlcBundle\Util\Interfaces\MarkerInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity()
 * @ORM\Table(name="button_type")
 */
class ButtonType
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="text", length=50)
     * @Assert\NotBlank()
     * @Assert\Length(max = 50)
     */
    private $name;

    /**
     * @ORM\Column(type="text", length=50)
     * @Assert\NotBlank()
     * @Assert\Length(max = 50)
     */
    private $action;

    /**
     * @ORM\OneToMany(
     *      targetEntity = "Button",
     *      mappedBy = "buttonType"
     * )
     */
    private $button;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->button = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ButtonType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set action
     *
     * @param string $action
     *
     * @return ButtonType
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Add button
     *
     * @param \Miuze\PlcBundle\Entity\Button $button
     *
     * @return ButtonType
     */
    public function addButton(\Miuze\PlcBundle\Entity\Button $button)
    {
        $this->button[] = $button;

        return $this;
    }

    /**
     * Remove button
     *
     * @param \Miuze\PlcBundle\Entity\Button $button
     */
    public function removeButton(\Miuze\PlcBundle\Entity\Button $button)
    {
        $this->button->removeElement($button);
    }

    /**
     * Get button
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getButton()
    {
        return $this->button;
    }
}
