<?php

declare(strict_types=1);

namespace Miuze\PlcBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Miuze\PlcBundle\Repository\RegisterRepository")
 * @ORM\Table(name="register")
 */
class Register
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=600)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=600
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=4)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max=4
     * )
     * @Assert\Range(
     *      min = 0001,
     *      max = 4168,
     *      minMessage = "Nie może być mniejszy niż {{ limit }}",
     *      maxMessage = "Nie może być większy niż {{ limit }}"
     * )
     */
    private $registerNumber;


    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $value;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Miuze\PlcBundle\Entity\Marker",
     *      inversedBy = "register"
     * )
     * @ORM\JoinColumn(
     *      name = "marker_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $marker;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Register
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getRegisterNumber()
    {
        if ($this->registerNumber === null) {
            return $this->registerNumber;
        } else {
            return str_pad((string)$this->registerNumber, 4, "0", STR_PAD_LEFT);
        }
    }

    /**
     * @param mixed $registerNumber
     */
    public function setRegisterNumber($registerNumber)
    {
        $this->registerNumber = $registerNumber;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * Set marker.
     *
     * @param \Miuze\PlcBundle\Entity\Marker|null $marker
     *
     * @return Register
     */
    public function setMarker(\Miuze\PlcBundle\Entity\Marker $marker = null)
    {
        $this->marker = $marker;

        return $this;
    }

    /**
     * Get marker.
     *
     * @return \Miuze\PlcBundle\Entity\Marker|null
     */
    public function getMarker()
    {
        return $this->marker;
    }
}
