<?php

declare(strict_types=1);

namespace Miuze\PlcBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="Miuze\PlcBundle\Repository\LogRepository")
 * @ORM\Table(name="log_marker")
 */
class Log
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createDate;

    /**
     * @ORM\Column(type="boolean")
     */
    private $value;

    /**
     * Constructor
     */
    public function __construct(UserInterface $user, Marker $marker)
    {
        $this->createDate = new \DateTime('now');
        $this->setUser($user);
        $this->setMarker($marker);
        $this->setValue($marker->getValue());
    }

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Miuze\UserBundle\Entity\User",
     * )
     * @ORM\JoinColumn(
     *      name = "user",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $user;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Miuze\PlcBundle\Entity\Marker",
     * )
     * @ORM\JoinColumn(
     *      name = "marker",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $marker;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createDate.
     *
     * @param \DateTime $createDate
     *
     * @return Log
     */
    private function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate.
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set value.
     *
     * @param bool $value
     *
     * @return Log
     */
    private function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value.
     *
     * @return bool
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set user.
     *
     * @param \Miuze\UserBundle\Entity\User|null $user
     *
     * @return Log
     */
    private function setUser(\Miuze\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \Miuze\UserBundle\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set marker.
     *
     * @param \Miuze\PlcBundle\Entity\Marker|null $marker
     *
     * @return Log
     */
    private function setMarker(\Miuze\PlcBundle\Entity\Marker $marker = null)
    {
        $this->marker = $marker;

        return $this;
    }

    /**
     * Get marker.
     *
     * @return \Miuze\
        $log = $this->get(MarkerLoger::class)PlcBundle\Entity\Marker|null
     */
    public function getMarker()
    {
        return $this->marker;
    }
}
