<?php

declare(strict_types=1);

namespace Miuze\PlcBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Miuze\PlcBundle\Util\Interfaces\MarkerInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="Miuze\PlcBundle\Repository\ButtonRepository")
 * @ORM\Table(name="buttons")
 */
class Button
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="text", length=50)
     * @Assert\NotBlank()
     * @Assert\Length(max = 50)
     */
    private $name;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "ButtonType",
     *      inversedBy = "button"
     * )
     * @ORM\JoinColumn(
     *      name = "button_type_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $buttonType;

    /**
     * @ORM\OneToMany(
     *      targetEntity = "ButtonMarker",
     *      mappedBy = "button",
     *      cascade={"persist", "merge"}
     * )
     */
    private $buttonMarker;

    /**
     * @ORM\OneToMany(
     *      targetEntity = "Miuze\RoomBundle\Entity\ViewItem",
     *      mappedBy = "button",
     * )
     */
    private $view;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->buttonMarker = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Button
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set buttonType
     *
     * @param \Miuze\PlcBundle\Entity\ButtonType $buttonType
     *
     * @return Button
     */
    public function setButtonType(\Miuze\PlcBundle\Entity\ButtonType $buttonType = null)
    {
        $this->buttonType = $buttonType;

        return $this;
    }

    /**
     * Get buttonType
     *
     * @return \Miuze\PlcBundle\Entity\ButtonType
     */
    public function getButtonType()
    {
        return $this->buttonType;
    }

    /**
     * Add buttonMarker
     *
     * @param \Miuze\PlcBundle\Entity\ButtonMarker $buttonMarker
     *
     * @return Button
     */
    public function addButtonMarker(\Miuze\PlcBundle\Entity\ButtonMarker $buttonMarker)
    {
        $this->buttonMarker[] = $buttonMarker;
        $buttonMarker->setButton($this);

        return $this;
    }

    /**
     * Remove buttonMarker
     *
     * @param \Miuze\PlcBundle\Entity\ButtonMarker $buttonMarker
     */
    public function removeButtonMarker(\Miuze\PlcBundle\Entity\ButtonMarker $buttonMarker)
    {
        $this->buttonMarker->removeElement($buttonMarker);
    }

    /**
     * Get buttonMarker
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getButtonMarker()
    {
        return $this->buttonMarker;
    }



    /**
     * Add view
     *
     * @param \Miuze\RoomBundle\Entity\ViewButtons $view
     *
     * @return Button
     */
    public function addView(\Miuze\RoomBundle\Entity\ViewItem $view)
    {
        $this->view[] = $view;

        return $this;
    }

    /**
     * Remove view
     *
     * @param \Miuze\RoomBundle\Entity\ViewItem $view
     */
    public function removeView(\Miuze\RoomBundle\Entity\ViewItem $view)
    {
        $this->view->removeElement($view);
    }

    /**
     * Get view
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getView()
    {
        return $this->view;
    }
}
