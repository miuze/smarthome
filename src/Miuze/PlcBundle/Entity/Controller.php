<?php

declare(strict_types=1);

namespace Miuze\PlcBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Class Controller
 * @package Miuze\PlcBundle\Entity
 * @ORM\Entity()
 * @ORM\Table(name="controllers")
 * @ORM\HasLifecycleCallbacks
 *
 * @UniqueEntity(fields={"ip"})
 */
class Controller
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     * @Assert\Length(max = 1)
     * @Assert\Type("int")
     */
    private $unit;

    /**
    * @ORM\Column(name="ip", type="string", length = 15)
    * @Assert\NotBlank
    * @Assert\Ip()
    */
    private $ip;

    /**
     * @ORM\Column(type="text", length=50)
     * @Assert\NotBlank()
     * @Assert\Length(max = 50)
     */
    private $name;

    /**
     * @ORM\OneToMany(
     *      targetEntity = "Marker",
     *      mappedBy = "controller"
     * )
     */
    private $markers;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set unit
     *
     * @param integer $unit
     *
     * @return Controller
     */
    public function setUnit(int $unit)
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * Get unit
     *
     * @return integer
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * Set ip
     *
     * @param string $ip
     *
     * @return Controller
     */
    public function setIp(string $ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Controller
     */
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

}
