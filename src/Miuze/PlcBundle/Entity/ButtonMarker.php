<?php

declare(strict_types=1);

namespace Miuze\PlcBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Miuze\PlcBundle\Util\Interfaces\MarkerInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="Miuze\PlcBundle\Repository\ButtonMarkerRepository")
 * @ORM\Table(name="button_marker")
 */
class ButtonMarker
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="text", length=50)
     * @Assert\NotBlank()
     * @Assert\Length(max = 50)
     */
    private $name;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Marker",
     *      inversedBy = "buttons"
     * )
     * @ORM\JoinColumn(
     *      name = "marker_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $marker;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "ButtonAction",
     *      inversedBy = "buttonMarker"
     * )
     * @ORM\JoinColumn(
     *      name = "button_action_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL"
     * )
     */
    private $markerAction;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Button",
     *      inversedBy = "buttonMarker"
     * )
     * @ORM\JoinColumn(
     *      name = "button_id",
     *      referencedColumnName= "id",
     *      onDelete = "SET NULL",
     * )
     */
    private $button;

    /**
     * @ORM\OneToMany(
     *      targetEntity = "Miuze\SceneBundle\Entity\SceneMarker",
     *      mappedBy = "marker",
     *      cascade={"all"}
     * )
     */
    protected $scene;

    /**
     * @ORM\OneToMany(
     *      targetEntity = "Miuze\NfcBundle\Entity\ActionMarker",
     *      mappedBy = "marker",
     *      cascade={"all"}
     * )
     */
    protected $nfc;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->scene = new \Doctrine\Common\Collections\ArrayCollection();
        $this->nfc = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return ButtonMarker
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set marker.
     *
     * @param \Miuze\PlcBundle\Entity\Marker|null $marker
     *
     * @return ButtonMarker
     */
    public function setMarker(\Miuze\PlcBundle\Entity\Marker $marker = null)
    {
        $this->marker = $marker;

        return $this;
    }

    /**
     * Get marker.
     *
     * @return \Miuze\PlcBundle\Entity\Marker|null
     */
    public function getMarker()
    {
        return $this->marker;
    }

    /**
     * Set markerAction.
     *
     * @param \Miuze\PlcBundle\Entity\ButtonAction|null $markerAction
     *
     * @return ButtonMarker
     */
    public function setMarkerAction(\Miuze\PlcBundle\Entity\ButtonAction $markerAction = null)
    {
        $this->markerAction = $markerAction;

        return $this;
    }

    /**
     * Get markerAction.
     *
     * @return \Miuze\PlcBundle\Entity\ButtonAction|null
     */
    public function getMarkerAction()
    {
        return $this->markerAction;
    }

    /**
     * Set button.
     *
     * @param \Miuze\PlcBundle\Entity\Button|null $button
     *
     * @return ButtonMarker
     */
    public function setButton(\Miuze\PlcBundle\Entity\Button $button = null)
    {
        $this->button = $button;

        return $this;
    }

    /**
     * Get button.
     *
     * @return \Miuze\PlcBundle\Entity\Button|null
     */
    public function getButton()
    {
        return $this->button;
    }

    /**
     * Add scene.
     *
     * @param \Miuze\SceneBundle\Entity\SceneMarker $scene
     *
     * @return ButtonMarker
     */
    public function addScene(\Miuze\SceneBundle\Entity\SceneMarker $scene)
    {
        $this->scene[] = $scene;

        return $this;
    }

    /**
     * Remove scene.
     *
     * @param \Miuze\SceneBundle\Entity\SceneMarker $scene
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeScene(\Miuze\SceneBundle\Entity\SceneMarker $scene)
    {
        return $this->scene->removeElement($scene);
    }

    /**
     * Get scene.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getScene()
    {
        return $this->scene;
    }

    /**
     * Add nfc.
     *
     * @param \Miuze\NfcBundle\Entity\ActionMarker $nfc
     *
     * @return ButtonMarker
     */
    public function addNfc(\Miuze\NfcBundle\Entity\ActionMarker $nfc)
    {
        $this->nfc[] = $nfc;

        return $this;
    }

    /**
     * Remove nfc.
     *
     * @param \Miuze\NfcBundle\Entity\ActionMarker $nfc
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeNfc(\Miuze\NfcBundle\Entity\ActionMarker $nfc)
    {
        return $this->nfc->removeElement($nfc);
    }

    /**
     * Get nfc.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNfc()
    {
        return $this->nfc;
    }
}
