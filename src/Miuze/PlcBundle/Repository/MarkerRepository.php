<?php

declare(strict_types=1);

namespace Miuze\PlcBundle\Repository;

use Doctrine\ORM\EntityRepository;

class MarkerRepository extends EntityRepository {

    public function getMinAndMaxAddress($unitId) {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('MIN(m.addressStatus) AS first, MAX(m.addressStatus) AS last')
            ->from('MiuzePlcBundle:Marker', 'm')
            ->innerJoin('m.controller', 'c')
            ->where('c.id = :unitId')
            ->setParameter('unitId', $unitId);

        $result = $qb->getQuery()->getOneOrNullResult();
        return $result;
    }

}
