<?php

declare(strict_types=1);

namespace Miuze\PlcBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ButtonMarkerRepository extends EntityRepository {

    public function getButton($id) {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('b, action, m')
            ->from('MiuzePlcBundle:ButtonMarker', 'b')
            ->leftJoin('b.markerAction', 'action')
            ->leftJoin('b.marker', 'm')
            ->where('b.id = :id')
            ->setParameter('id', $id);

        return $qb->getQuery()->getOneOrNullResult();
    }

}
