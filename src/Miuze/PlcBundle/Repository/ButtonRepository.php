<?php

declare(strict_types=1);

namespace Miuze\PlcBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;

class ButtonRepository extends EntityRepository {

    public function getButton($id) {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('b, bm, type, marker, action')
            ->from('MiuzePlcBundle:Button', 'b')
            ->leftJoin('b.buttonMarker', 'bm')
            ->leftJoin('b.buttonType', 'type')
            ->leftJoin('bm.markerAction', 'action')
            ->leftJoin('bm.marker', 'marker')
            ->where('b.id = :id')
            ->setParameter('id', $id);

            return $qb->getQuery()->getOneOrNullResult();
    }

}
