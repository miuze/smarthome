<?php

declare(strict_types=1);

namespace Miuze\PlcBundle\Repository;

use Doctrine\ORM\EntityRepository;

class LogRepository extends EntityRepository {

    public function getQueryPagination() {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('l, m, u')
            ->from('MiuzePlcBundle:Log', 'l')
            ->leftJoin('l.marker', 'm')
            ->leftJoin('l.user', 'u')
            ->orderBy('l.createDate', 'DESC');

        return $qb->getQuery();
    }

}
