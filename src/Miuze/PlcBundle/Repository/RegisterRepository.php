<?php
namespace Miuze\PlcBundle\Repository;

use Doctrine\ORM\EntityRepository;

class RegisterRepository extends EntityRepository
{
    public function findOn(){
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('o, s, r')
            ->from('MiuzePlcBundle:Marker', 'm')
            ->where('m.value = 1');

        return $qb->getQuery()->getResult();;
    }
}
