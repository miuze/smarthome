<?php

declare(strict_types=1);

namespace Miuze\PlcBundle\Service;

use Symfony\Component\HttpFoundation\Session\Session;
use Miuze\PlcBundle\Util\Enum\MessageStatus;

class Message
{
    private $session;

    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    public function setMessage(string $status, string $msg = ''): void
    {
        $this->session->getFlashBag()->add($status, $msg);
    }
}