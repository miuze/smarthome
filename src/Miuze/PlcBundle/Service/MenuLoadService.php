<?php

declare(strict_types=1);

namespace Miuze\PlcBundle\Service;

use Miuze\UserBundle\Util\MenuInterface;
use Symfony\Component\Yaml\Yaml;

class MenuLoadService implements MenuInterface
{
    public function generate(): array
    {
        return Yaml::parseFile(__DIR__ . '/../Resources/config/menu.yml');
    }
}