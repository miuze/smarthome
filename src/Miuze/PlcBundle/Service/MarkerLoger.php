<?php

declare(strict_types=1);

namespace Miuze\PlcBundle\Service;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Miuze\PlcBundle\Entity\Marker;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Miuze\PlcBundle\Entity\Log;

class MarkerLoger
{
    /**
     * @var Registry
     */
    private $em;

    /**
     * @var Security
     */
    private $user;

    public function __construct(Registry $doctrine, TokenStorageInterface $tokenStorage)
    {
        $this->em = $doctrine->getManager();
        $this->user = $tokenStorage->getToken()->getUser();
    }

    public function addLog(Marker $marker){
        $log = new Log($this->user, $marker);
        $this->em->persist($log);
        $this->em->flush();
    }

}