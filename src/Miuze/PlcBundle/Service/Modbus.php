<?php

declare(strict_types=1);

namespace Miuze\PlcBundle\Service;


use Miuze\PlcBundle\Entity\ButtonMarker;
use Miuze\PlcBundle\Entity\Controller;
use Miuze\PlcBundle\Entity\Register;
use Miuze\PlcBundle\Util\Enum\MarkerStatus;
use Miuze\PlcBundle\Util\Enum\MessageStatus;
use Miuze\PlcBundle\Util\Interfaces\MarkerInterface;
use Miuze\PlcBundle\Util\Modbus\ModbusMaster;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Miuze\PlcBundle\Service\Message;

class Modbus
{
    private $value = true;

    private $doctrine;

    private $message;

    public function __construct(Registry $doctrine, Message $message)
    {
        $this->doctrine = $doctrine;
        $this->message = $message;
    }

    public function sendMarkerToPlc(MarkerInterface $marker, $saveStatus = true)
    {
        try{
            $modbus = new ModbusMaster($marker->getController()->getIp());
            if($marker->getAddress() === $marker->getAddressStatus()) {
                $modbus->writeSingleCoil(0, $marker->getAddress(), [$marker->getValue()]);
            }else{
                $modbus->writeSingleCoil(0, $marker->getAddress(), [true]);
            }
            $em = $this->doctrine->getManager();
            $em->merge($marker);
            $em->flush();
            $this->message->setMessage(
                MessageStatus::SUCCESS,
                sprintf('%s wejście %s.', $this->getValueString($marker), strtolower($marker->getName()))
            );
            return true;
        } catch (\Exception $e){
            $this->message->setMessage(MessageStatus::DANGER, $e->getMessage());
            return false;
        }
    }

    public function sendStatusMarkerToPlc(MarkerInterface $marker, bool $value)
    {
        try{
            $modbus = new ModbusMaster($marker->getController()->getIp());
            $modbus->writeSingleCoil(0, $marker->getAddress(), [$value]);
            $em = $this->doctrine->getManager();
            $em->merge($marker);
            $em->flush();

            return true;
        } catch (\Exception $e){
            return false;
        }
    }

    public function getMarkersFromPlc(Controller $controller, $startAddress = 0, $quantity = 255)
    {
        try {
            $this->modbus = new ModbusMaster($controller->getIp());
            $result = $this->modbus->readCoils($controller->getUnit(), $startAddress, $quantity);
            return $result;
        } catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function getValueToSend(ButtonMarker $button):bool
    {
        if(
            $button->getMarkerAction()->getAlwaysTrue() ||
            $button->getMarker()->getAddress() !== $button->getMarker()->getAddressStatus()
        ){
            return $this->value;
        }

        $this->value = $button->getMarker()->getValue() === true ? false : true;
        return $this->value;
    }

    public function writeRegister(Register $register)
    {
        $controller = $register->getMarker()->getController();
        try{
            $modbus = new ModbusMaster($controller->getIp());
            $modbus->writeSingleRegister($controller->getUnit(), $register->getRegisterNumber(),  [$register->getValue()], ['INT']);
            $em = $this->doctrine->getManager();
            $em->merge($register);
            $em->flush();
            $this->message->setMessage(
                MessageStatus::SUCCESS,
                sprintf('Rejestr %s zapisany poprawnie.', strtolower($register->getName()))
            );
            return true;
        } catch (\Exception $e){
            $this->message->setMessage(MessageStatus::DANGER, $e->getMessage());
            return false;
        }
    }

    public function readRegister(Register $register)
    {
        $controller = $register->getMarker()->getController();
        try{
            $modbus = new ModbusMaster($controller->getIp());
            $value = $modbus->readMultipleRegisters($controller->getUnit(), $register->getRegisterNumber(), 1);
            $register->setValue($value);
            $em = $this->doctrine->getManager();
            $em->merge($register);
            $em->flush();
            $this->message->setMessage(
                MessageStatus::SUCCESS,
                sprintf('Rejestr %s zapisany poprawnie.', strtolower($register->getName()))
            );
            return true;
        } catch (\Exception $e){
            $this->message->setMessage(MessageStatus::DANGER, $e->getMessage());
            return false;
        }
    }

    public function getValueString()
    {
        if($this->value === true){
            return MarkerStatus::ON;
        }else{
            return MarkerStatus::OFF;
        }
    }

}
