<?php

namespace Miuze\PlcBundle\Command;

use Doctrine\ORM\EntityManager;
use Miuze\PlcBundle\Entity\Controller;
use Miuze\PlcBundle\Entity\Marker;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MarkerCommand extends ContainerAwareCommand
{

    /**
     * @var Miuze\PlcBundle\Service\Modbus
     */
    private $modbusService;

    /**
     * @var EntityManager
     */
    private $em;

    private $plcMarkers = [];
    private $updatedMarkers = [];

    protected function configure()
    {
        $this
            ->setName('miuze:plc:sync')
            ->setDescription('Update markers database from controllers');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $this->em = $doctrine->getEntityManager();
        $this->modbusService = $this->getContainer()->get('Miuze\PlcBundle\Service\Modbus');
        $units = $doctrine->getRepository("MiuzePlcBundle:Controller")->findAll();
        $markers = $this->getContainer()->get('doctrine')->getRepository("MiuzePlcBundle:Marker")->findAll();
        foreach ($units as $unit) {
            $minAndMax = $doctrine->getRepository("MiuzePlcBundle:Marker")->getMinAndMaxAddress($unit->getId());
            $this->buildPLCMarkersArray($minAndMax['first'], $minAndMax['last'], $unit);
        }
        foreach ($markers as $marker) {
            $this->checkValue($marker);
        }
        $this->em->flush();
        $output->writeln(sprintf('Zaktualizowano rekordów output: %d', count($this->updatedMarkers)));
    }

    private function buildPLCMarkersArray(int $min, int $max, Controller $unitId): void
    {
        $startMarkerNumber = str_pad($min, 4, "0", STR_PAD_LEFT);
        $quantity = $max - $min;
        $markers = $this->modbusService->getMarkersFromPlc($unitId, $startMarkerNumber, $quantity);
        foreach ($markers as $key => $value) {
            $markerNumber = str_pad($min + $key, 4, "0", STR_PAD_LEFT);
            $this->plcMarkers[$markerNumber] = $value;
        }
    }


    private function checkValue(Marker $marker):void
    {
        if(isset($this->plcMarkers[$marker->getAddressStatus()])) {
            $valMarker = $this->plcMarkers[$marker->getAddressStatus()];
            if ($valMarker != $marker->getValue()) {
                $marker->setValue($valMarker);
                $this->em->merge($marker);
                array_push($this->updatedMarkers, $marker);
            }
        }
    }
}
