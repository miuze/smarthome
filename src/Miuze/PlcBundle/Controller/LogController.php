<?php

namespace Miuze\PlcBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/log")
 */
class LogController extends DefaultController
{
    public $defaultRoutePlc = 'miuze_plc_log_index';

    /**
     * @Route(
     *      "/",
     *      name="miuze_plc_log_index",
     * )
     */
    public function indexAction(Request $request)
    {
        $query = $this->getDoctrine()->getRepository('MiuzePlcBundle:Log')->getQueryPagination();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            $request->query->getInt('offset', 10),
        );

        return $this->view(
            '@MiuzePlc/Log/index.html.twig',
            [
                'action' => 'Logi markerów',
                'pagination' => $pagination,
            ]
        );
    }
}
