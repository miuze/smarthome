<?php

namespace Miuze\PlcBundle\Controller;

use Miuze\PlcBundle\Controller\DefaultController;
use Miuze\PlcBundle\Entity\ButtonAction;
use Miuze\PlcBundle\Form\ButtonAction\ButtonActionType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
* @Route("/button-action")
*/
class ButtonActionController extends DefaultController
{
    public $defaultRoutePlc = 'miuze_plc_button-action_index';

    /**
     * @Route(
     *      "/",
     *      name="miuze_plc_button-action_index"
     * )
     */
    public function indexAction()
    {
        $list = $this->getDoctrine()->getRepository('MiuzePlcBundle:ButtonAction')->findAll();

        return $this->view(
            '@MiuzePlc/ButtonAction/index.html.twig',
            [
                'action' => 'Lista akcji',
                'list' => $list,
            ]
        );
    }
    
    /**
     * @Route(
     *      "/add",
     *      name="miuze_plc_button-action_add"
     * )
     */
    public function addAction(Request $request){
        
        $entity = new ButtonAction();
        $form = $this->createForm(ButtonActionType::class, $entity);
        if($request->isMethod(Request::METHOD_POST)){
            $form->handleRequest($request);
            if($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                $this->setMessage('success', 'Zapisano ustawienia');

                return $this->redirect($this->generateUrl($this->defaultRoutePlc));
            }
        }

        return $this->view(
            '@MiuzePlc/ButtonAction/add.html.twig',
            [
                'action' => 'Dodaj akcję',
                'form' => $form->createView(),
            ]
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name="miuze_plc_button-action_edit"
     * )
     */
    public function editAction(Request $request){
        
        $entity = $this->getDoctrine()
            ->getRepository('MiuzePlcBundle:ButtonAction')
            ->findOneById($request->attributes->getInt('id'));
        if($entity == null){
            $this->setMessage('danger', 'Nie znaleziono akcji');

            return $this->redirect($this->generateUrl($this->defaultRoutePlc));
        }
        $form = $this->createForm(ButtonActionType::class, $entity);
        if($request->isMethod(Request::METHOD_POST)){
            $form->handleRequest($request);
            if($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $em->merge($entity);
                $em->flush();
                $this->setMessage('success', 'Zapisano ustawienia');

                return $this->redirect($this->generateUrl($this->defaultRoutePlc));
            }
        }

        return $this->view(
            '@MiuzePlc/ButtonAction/add.html.twig',
            [
                'action' => 'Edycja akcji',
                'form' => $form->createView(),
            ]
        );
    }
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="miuze_plc_button-action_delete"
     * )
     */
    public function deleteAction(Request $request){
        
        $entity = $this->getDoctrine()
            ->getRepository('MiuzePlcBundle:ButtonAction')
            ->findOneById($request->attributes->getInt('id'));
        if($entity == null){
            $this->setMessage('danger', 'Nie znaleziono akcji');

            return $this->redirect($this->generateUrl($this->defaultRoutePlc));
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();
        $this->setMessage('success', 'Akcja usunięty');

        return $this->redirect($this->generateUrl($this->defaultRoutePlc));
    }
}
