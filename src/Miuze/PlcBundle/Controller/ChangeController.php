<?php

declare(strict_types=1);

namespace Miuze\PlcBundle\Controller;

use Miuze\PlcBundle\Service\MarkerLoger;
use Miuze\PlcBundle\Util\Enum\MessageStatus;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Miuze\PlcBundle\Service\Modbus;

/**
 * @Route("/change")
 */
class ChangeController extends Controller
{
    /**
     * @Route(
     *      "/{id}",
     *      name="miuze_plc_change",
     *     defaults={"id" = 0},
     *     requirements={"id"="\d+"}
     * )
     */
    public function indexAction(Request $request)
    {
        $id = (int) $request->get('buttonMarker') != 0 ? (int) $request->get('buttonMarker') : $request->attributes->getInt('id');
        $modbusService = $this->get(Modbus::class);
        $log = $this->get(MarkerLoger::class);

        $entity = $this->getDoctrine()->getRepository('MiuzePlcBundle:ButtonMarker')->getButton($id);
        $entity->getMarker()->setValue($modbusService->getValueToSend($entity));

        if($modbusService->sendMarkerToPlc($entity->getMarker())){
            $log->addLog($entity->getMarker());
            $msg = sprintf('%s wejście %s.', $modbusService->getValueString(), strtolower($entity->getMarker()->getName()));

            return new JsonResponse($msg);
        }else{

            return new JsonResponse('Błąd komunikacji ze sterownikiem', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
