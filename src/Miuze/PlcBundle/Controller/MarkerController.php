<?php

namespace Miuze\PlcBundle\Controller;

use Miuze\PlcBundle\Controller\DefaultController;
use Miuze\PlcBundle\Entity\Marker;
use Miuze\PlcBundle\Form\Marker\MarkerType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
* @Route("/marker")
*/
class MarkerController extends DefaultController
{
    public $defaultRoutePlc = 'miuze_plc_marker_index';

    /**
     * @Route(
     *      "/",
     *      name="miuze_plc_marker_index"
     * )
     */
    public function indexAction()
    {
        $list = $this->getDoctrine()->getRepository('MiuzePlcBundle:Marker')->findAll();

        return $this->view(
            '@MiuzePlc/Marker/index.html.twig',
            [
                'action' => 'Lista markerów',
                'list' => $list,
            ]
        );
    }
    
    /**
     * @Route(
     *      "/add",
     *      name="miuze_plc_marker_add"
     * )
     */
    public function addAction(Request $request){
        
        $entity = new Marker();
        $form = $this->createForm(MarkerType::class, $entity);
        if($request->isMethod(Request::METHOD_POST)){
            $form->handleRequest($request);
            if($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                $this->setMessage('success', 'Zapisano ustawienia');

                return $this->redirect($this->generateUrl($this->defaultRoutePlc));
            }
        }

        return $this->view(
            '@MiuzePlc/Marker/add.html.twig',
            [
                'action' => 'Dodaj marker',
                'form' => $form->createView(),
            ]
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name="miuze_plc_marker_edit"
     * )
     */
    public function editAction(Request $request){
        
        $entity = $this->getDoctrine()
            ->getRepository('MiuzePlcBundle:Marker')
            ->findOneById($request->attributes->getInt('id'));
        if($entity == null){
            $this->setMessage('danger', 'Nie znaleziono markera');

            return $this->redirect($this->generateUrl($this->defaultRoutePlc));
        }
        $form = $this->createForm(MarkerType::class, $entity);
        if($request->isMethod(Request::METHOD_POST)){
            $form->handleRequest($request);
            if($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $em->merge($entity);
                $em->flush();
                $this->setMessage('success', 'Zapisano ustawienia');

                return $this->redirect($this->generateUrl($this->defaultRoutePlc));
            }
        }

        return $this->view(
            '@MiuzePlc/Marker/add.html.twig',
            [
                'action' => 'Edycja markera',
                'form' => $form->createView(),
            ]
        );
    }
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="miuze_plc_marker_delete"
     * )
     */
    public function deleteAction(Request $request){
        
        $entity = $this->getDoctrine()
            ->getRepository('MiuzePlcBundle:Marker')
            ->findOneById($request->attributes->getInt('id'));
        if($entity == null){
            $this->setMessage('danger', 'Nie znaleziono markera');

            return $this->redirect($this->generateUrl($this->defaultRoutePlc));
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();
        $this->setMessage('success', 'Marker usunięty');

        return $this->redirect($this->generateUrl($this->defaultRoutePlc));
    }

}
