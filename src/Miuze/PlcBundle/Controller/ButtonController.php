<?php

namespace Miuze\PlcBundle\Controller;

use Miuze\PlcBundle\Entity\Button;
use Miuze\PlcBundle\Form\Button\ButtonType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/button")
 */
class ButtonController extends DefaultController
{
    public $defaultRoutePlc = 'miuze_plc_button_index';

    /**
     * @Route(
     *      "/",
     *      name="miuze_plc_button_index"
     * )
     */
    public function indexAction()
    {
        $list = $this->getDoctrine()->getRepository('MiuzePlcBundle:Button')->findAll();

        return $this->view(
            '@MiuzePlc/Button/index.html.twig',
            [
                'action' => 'Lista przycisków',
                'list' => $list,
            ]
        );
    }

    /**
     * @Route(
     *      "/add",
     *      name="miuze_plc_button_add"
     * )
     */
    public function addAction(Request $request)
    {
        $entity = new Button();
        $form = $this->createForm(ButtonType::class, $entity);
        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                $this->setMessage('success', 'Zapisano ustawienia');

                return $this->redirect($this->generateUrl($this->defaultRoutePlc));
            }
        }

        return $this->view(
            '@MiuzePlc/Button/add.html.twig',
            [
                'action' => 'Dodaj przycisk',
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route(
     *      "/edit/{id}",
     *      name="miuze_plc_button_edit"
     * )
     */
    public function editAction(Request $request)
    {
        $entity = $this->getDoctrine()
            ->getRepository('MiuzePlcBundle:Button')
            ->findOneById($request->attributes->getInt('id'));
        if ($entity == null) {
            $this->setMessage('danger', 'Nie znaleziono przycisku');

            return $this->redirect($this->generateUrl($this->defaultRoutePlc));
        }

        $form = $this->createForm(ButtonType::class, $entity);
        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->merge($entity);
                $em->flush();
                $this->setMessage('success', 'Zapisano ustawienia');

                return $this->redirect($this->generateUrl($this->defaultRoutePlc));
            }
        }

        return $this->view(
            '@MiuzePlc/Button/add.html.twig',
            [
                'action' => 'Edycja przycisku',
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route(
     *      "/delete/{id}",
     *      name="miuze_plc_button_delete"
     * )
     */
    public function deleteAction(Request $request)
    {
        $entity = $this->getDoctrine()
            ->getRepository('MiuzePlcBundle:Button')
            ->findOneById($request->attributes->getInt('id'));
        if ($entity == null) {
            $this->setMessage('danger', 'Nie znaleziono akcji');

            return $this->redirect($this->generateUrl($this->defaultRoutePlc));
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();
        $this->setMessage('success', 'Przycisk usunięty');

        return $this->redirect($this->generateUrl($this->defaultRoutePlc));
    }

    /**
     * @Route(
     *      "/copy/{id}",
     *      name="miuze_plc_button_copy"
     * )
     */
    public function copyAction(Request $request)
    {
        $entity = $this->getDoctrine()
            ->getRepository('MiuzePlcBundle:Button')
            ->findOneById($request->attributes->getInt('id'));
        if ($entity == null) {
            $this->setMessage('danger', 'Nie znaleziono przycisku');

            return $this->redirect($this->generateUrl($this->defaultRoutePlc));
        }
        $entity = clone $entity;
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
        $this->setMessage('success', 'Skopiowano ustawienia');

        return $this->redirect($this->generateUrl('miuze_plc_button_edit', array('id' => $entity->getId())));
    }

}
