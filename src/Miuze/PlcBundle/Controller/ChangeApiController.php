<?php

namespace Miuze\PlcBundle\Controller;

use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\FOSRestController;
use Miuze\PlcBundle\Service\MarkerLoger;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/plc")
 */
class ChangeApiController extends FOSRestController
{
    /**
     * @Put("/change", name="_api_plc_change")
     * @SWG\Response(
     *     response=200,
     *     description="Okiekt dodany poprawnie"
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Błąd parametrów",
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Blędna autoryzacja",
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Blędna komunikacja ze sterownikiem",
     * )
     * @SWG\Parameter(
     *     name="payload",
     *     in="body",
     *     required=true,
     *     description="Zmiana stanu wyjśc przypisanych do ButttonMarkera",
     *     @SWG\Schema(
     *          @SWG\Property(
     *              property="buttonMarker",
     *              type="integer",
     *              example=1,
     *              description="Id ButtonMarker"
     *          )
     *      ),
     * )
     * @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     required=true,
     *     type="string",
     *     description="Token z logowania",
     *     )
     * @SWG\Tag(name="Plc")
     */
    public function putAction(Request $request)
    {
        $id = $request->get('buttonMarker');
        if (is_null($id)) {
            return new JsonResponse('Brak parametru', Response::HTTP_BAD_REQUEST);
        }
        $modbusService = $this->get('Miuze\PlcBundle\Service\Modbus');
        $log = $this->get(MarkerLoger::class);
        $entity = $this->getDoctrine()->getRepository('MiuzePlcBundle:ButtonMarker')->getButton($id);
        $entity->getMarker()->setValue($modbusService->getValueToSend($entity));
        if ($modbusService->sendMarkerToPlc($entity->getMarker())) {
            $log->addLog($entity->getMarker());
            return new JsonResponse('Wykonano', Response::HTTP_OK);
        } else {

            return new JsonResponse('Błąd komunikacji ze sterownikiem', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
