<?php

namespace Miuze\PlcBundle\Controller;

use Miuze\PlcBundle\Controller\DefaultController;
use Miuze\PlcBundle\Entity\Controller;
use Miuze\PlcBundle\Form\Plc\PlcType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
* @Route("/plc")
*/
class PlcController extends DefaultController
{
    public $defaultRoutePlc = 'miuze_plc_controller_index';

    /**
     * @Route(
     *      "/",
     *      name="miuze_plc_controller_index"
     * )
     */
    public function indexAction()
    {
        $list = $this->getDoctrine()->getRepository('MiuzePlcBundle:Controller')->findAll();

        return $this->view(
            '@MiuzePlc/Plc/index.html.twig',
            [
                'action' => 'Lista sterowników',
                'list' => $list,
            ]
        );
    }
    
    /**
     * @Route(
     *      "/add",
     *      name="miuze_plc_controller_add"
     * )
     */
    public function addAction(Request $request)
    {
        $entity = new Controller();
        $form = $this->createForm(PlcType::class, $entity);
        if($request->isMethod(Request::METHOD_POST)){
            $form->handleRequest($request);
            if($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                $this->setMessage('success', 'Zapisano ustawienia');

                return $this->redirect($this->generateUrl($this->defaultRoutePlc));
            }
        }

        return $this->view(
            '@MiuzePlc/Plc/add.html.twig',
            [
                'action' => 'Dodaj sterownik',
                'form' => $form->createView(),
            ]
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name="miuze_plc_controller_edit"
     * )
     */
    public function editAction(Request $request){
        
        $entity = $this->getDoctrine()
            ->getRepository('MiuzePlcBundle:Controller')
            ->findOneById($request->attributes->getInt('id'));
        if($entity == null){
            $this->setMessage('danger', 'Nie znaleziono sterownika');

            return $this->redirect($this->generateUrl($this->defaultRoutePlc));
        }
        $form = $this->createForm(PlcType::class, $entity);
        if($request->isMethod(Request::METHOD_POST)){
            $form->handleRequest($request);
            if($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $em->merge($entity);
                $em->flush();
                $this->setMessage('success', 'Zapisano ustawienia');

                return $this->redirect($this->generateUrl($this->defaultRoutePlc));
            }
        }

        return $this->view(
            '@MiuzePlc/Plc/add.html.twig',
            [
                'action' => 'Edycja sterownika',
                'form' => $form->createView(),
            ]
        );
    }
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="miuze_plc_controller_delete"
     * )
     */
    public function deleteAction(Request $request){
        
        $entity = $this->getDoctrine()
            ->getRepository('MiuzePlcBundle:Controller')
            ->findOneById($request->attributes->getInt('id'));
        if($entity == null){
            $this->setMessage('danger', 'Nie znaleziono sterownika');

            return $this->redirect($this->generateUrl($this->defaultRoutePlc));
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();
        $this->setMessage('success', 'Sterownik usunięty');

        return $this->redirect($this->generateUrl($this->defaultRoutePlc));
    }

}
