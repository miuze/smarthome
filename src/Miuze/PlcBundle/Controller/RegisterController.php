<?php

namespace Miuze\PlcBundle\Controller;

use Miuze\PlcBundle\Controller\DefaultController;
use Miuze\PlcBundle\Entity\Register;
use Miuze\PlcBundle\Form\Register\RegisterEditType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Miuze\PlcBundle\Form\Register\RegisterType;

/**
 * @Route("/register")
 */
class RegisterController extends DefaultController {

    public $defaultRoutePlc = 'miuze_plc_register_index';
    /**
     * @Route(
     *      "/",
     *      name="miuze_plc_register_index"
     * )
     */
    public function indexAction() {
        $list = $this->getDoctrine()->getRepository('MiuzePlcBundle:Register')->findAll();

        return $this->view(
            '@MiuzePlc/Register/index.html.twig',
            [
                'action' => 'Lista rejestrów',
                'list' => $list,
            ]
        );
    }

    /**
     * @Route(
     *      "/add",
     *      name="miuze_plc_register_add"
     * )
     */
    public function addAction(Request $request)
    {
        $entity = new Register();
        $form = $this->createForm(RegisterType::class, $entity);
        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $modbusService = $this->get('Miuze\PlcBundle\Service\Modbus');
                $modbusService->readRegister($entity);

                return $this->redirect($this->generateUrl($this->defaultRoutePlc));
            } else {
                $this->setMessage('danger', 'Sprawdź formularz');
            }
        }

        return $this->view(
            '@MiuzePlc/Register/add.html.twig',
            [
                'action' => 'Dodaj rejestr',
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route(
     *      "/edit/{id}",
     *      name="miuze_plc_register_edit"
     * )
     */
    public function editAction(Request $request) {

        $entity = $this->getDoctrine()
            ->getRepository('MiuzePlcBundle:Register')
            ->findOneBy(array('id' => $request->attributes->getInt('id')));
        if ($entity == null) {
            $this->setMessage('danger', 'Nie znaleziono rejestru');

            return $this->redirect($this->generateUrl($this->defaultRoutePlc));
        }
        $form = $this->createForm(RegisterEditType::class, $entity);
        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $modbusService = $this->get('Miuze\PlcBundle\Service\Modbus');
                $modbusService->writeRegister($entity);

                return $this->redirect($this->generateUrl($this->defaultRoutePlc));
            } else {
                $this->setMessage('danger', 'Sprawdź formularz');
            }
        }

        return $this->view(
            '@MiuzePlc/Register/add.html.twig',
            [
                'action' => 'Edycja rejestru',
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route(
     *      "/delete/{id}",
     *      name="miuze_plc_register_delete"
     * )
     */
    public function deleteAction(Request $request) {

        $entity = $this->getDoctrine()->getRepository('MiuzePlcBundle:Register')
            ->findOneById($request->attributes->getInt('id'));
        if ($entity == null) {
            $this->setMessage('danger', 'Nie znaleziono rejestru');

            return $this->redirect($this->generateUrl($this->defaultRoutePlc));
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();
        $this->setMessage('success', 'Wyjście usunięte');

        return $this->redirect($this->generateUrl($this->defaultRoutePlc));
    }

}
