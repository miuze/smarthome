<?php

namespace Miuze\PlcBundle\Controller;

use Miuze\PlcBundle\Controller\DefaultController;
use Miuze\PlcBundle\Entity\ButtonType;
use Miuze\PlcBundle\Form\ButtonType\ButtonTypeType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
* @Route("/button-type")
*/
class ButtonTypeController extends DefaultController
{
    public $defaultRoutePlc = 'miuze_plc_button-type_index';

    /**
     * @Route(
     *      "/",
     *      name="miuze_plc_button-type_index"
     * )
     */
    public function indexAction()
    {
        $list = $this->getDoctrine()->getRepository('MiuzePlcBundle:ButtonType')->findAll();

        return $this->view(
            '@MiuzePlc/ButtonType/index.html.twig',
            [
                'action' => 'Lista typów',
                'list' => $list,
            ]
        );
    }
    
    /**
     * @Route(
     *      "/add",
     *      name="miuze_plc_button-type_add"
     * )
     */
    public function addAction(Request $request)
    {
        $entity = new ButtonType();
        $form = $this->createForm(ButtonTypeType::class, $entity);
        if($request->isMethod(Request::METHOD_POST)){
            $form->handleRequest($request);
            if($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                $this->setMessage('success', 'Zapisano ustawienia');

                return $this->redirect($this->generateUrl($this->defaultRoutePlc));
            }
        }

        return $this->view(
            '@MiuzePlc/ButtonType/add.html.twig',
            [
                'action' => 'Dodaj typ',
                'form' => $form->createView(),
            ]
        );
    }
    
    /**
     * @Route(
     *      "/edit/{id}",
     *      name="miuze_plc_button-type_edit"
     * )
     */
    public function editAction(Request $request){
        
        $entity = $this->getDoctrine()
            ->getRepository('MiuzePlcBundle:ButtonType')
            ->findOneById($request->attributes->getInt('id'));
        if($entity == null){
            $this->setMessage('danger', 'Nie znaleziono akcji');

            return $this->redirect($this->generateUrl($this->defaultRoutePlc));
        }
        $form = $this->createForm(ButtonTypeType::class, $entity);
        if($request->isMethod(Request::METHOD_POST)){
            $form->handleRequest($request);
            if($form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $em->merge($entity);
                $em->flush();
                $this->setMessage('success', 'Zapisano ustawienia');

                return $this->redirect($this->generateUrl($this->defaultRoutePlc));
            }
        }

        return $this->view(
            '@MiuzePlc/ButtonType/add.html.twig',
            [
                'action' => 'Edycja typu',
                'form' => $form->createView(),
            ]
        );
    }
    
    /**
     * @Route(
     *      "/delete/{id}",
     *      name="miuze_plc_button-type_delete"
     * )
     */
    public function deleteAction(Request $request){
        
        $entity = $this->getDoctrine()
            ->getRepository('MiuzePlcBundle:ButtonType')
            ->findOneById($request->attributes->getInt('id'));
        if($entity == null){
            $this->setMessage('danger', 'Nie znaleziono akcji');

            return $this->redirect($this->generateUrl($this->defaultRoutePlc));
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();
        $this->setMessage('success', 'Typ usunięty');

        return $this->redirect($this->generateUrl($this->defaultRoutePlc));
    }

}
