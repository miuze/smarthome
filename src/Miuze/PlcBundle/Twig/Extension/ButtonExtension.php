<?php

namespace Miuze\PlcBundle\Twig\Extension;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Miuze\PlcBundle\Entity\Button;

class ButtonExtension extends \Twig_Extension
{

    /**
     * @var Registry
     */
    private $doctrine;

    function __construct(Registry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function getName()
    {
        return 'miuze_plc_button_extension';
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction(
                'render_button', [$this, 'renderButton'],
                [
                    'is_safe' => ['html'],
                    'needs_environment' => true,
                ]
            ),
        );
    }

    public function renderButton(\Twig_Environment $environment, Button $button)
    {
        $repo = $this->doctrine->getRepository('MiuzePlcBundle:Button');
        $button = $repo->getButton($button->getId());
        if (\is_null($button)) {
            return;
        }
        if (\is_null($button->getButtonType())) {
            throw new \Exception('Nie ustawiono typu dla przycisku ' . $button->gerName());
        }

        return $environment->render(
            '@MiuzePlc/Twig/Buttons/' . $button->getButtonType()->getAction() . '.html.twig',
            array(
                'button' => $button
            )
        );
    }

}
